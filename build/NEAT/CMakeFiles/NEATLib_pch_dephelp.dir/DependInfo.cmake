# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/idgresearch/Desktop/SoftRobotsCode/Soft-Robots-Novelty-Search-master/cppn-neat/NE/HyperNEAT/build/NEAT/NEATLib_pch_dephelp.cxx" "/home/idgresearch/Desktop/SoftRobotsCode/Soft-Robots-Novelty-Search-master/cppn-neat/NE/HyperNEAT/build/NEAT/CMakeFiles/NEATLib_pch_dephelp.dir/NEATLib_pch_dephelp.cxx.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "BOOST_ALL_NO_LIB"
  "BUILD_NEAT_DLL"
  "NOMINMAX"
  "TIXML_USE_STL"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../NEAT/include"
  "../NEAT/../../../tinyxmldll/include"
  "../NEAT/../../../../Libraries/boost-trunk"
  "../NEAT/../../../JGTL/include"
  "../NEAT/../../../zlib"
  "../NEAT/../../../Board/include"
  "/usr/local/include/opencv"
  "/usr/local/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
