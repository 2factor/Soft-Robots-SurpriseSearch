# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/idgresearch/Desktop/SoftRobotsCode/Soft-Robots-Novelty-Search-master/cppn-neat/NE/HyperNEAT/Hypercube_NEAT/src/main.cpp" "/home/idgresearch/Desktop/SoftRobotsCode/Soft-Robots-Novelty-Search-master/cppn-neat/NE/HyperNEAT/build/Hypercube_NEAT/CMakeFiles/Hypercube_NEAT.dir/src/main.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "BOOST_ALL_NO_LIB"
  "BOOST_PYTHON_STATIC_LIB"
  "HCUBE_NOGUI"
  "NOMINMAX"
  "NOPCH"
  "TIXML_USE_STL"
  "WXUSINGDLL"
  "_USE_MATH_DEFINES"
  "__WXGTK__"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/idgresearch/Desktop/SoftRobotsCode/Soft-Robots-Novelty-Search-master/cppn-neat/NE/HyperNEAT/build/Hypercube_NEAT/CMakeFiles/Hypercube_NEAT_Base.dir/DependInfo.cmake"
  "/home/idgresearch/Desktop/SoftRobotsCode/Soft-Robots-Novelty-Search-master/cppn-neat/NE/HyperNEAT/build/NEAT/CMakeFiles/NEATLib.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/local/include/opencv"
  "/usr/local/include"
  "../Hypercube_NEAT/include"
  "../Hypercube_NEAT/../NEAT/include"
  "../Hypercube_NEAT/src/utils"
  "../Hypercube_NEAT/../../../tinyxmldll/include"
  "../Hypercube_NEAT/../../../../Libraries/boost-trunk"
  "../Hypercube_NEAT/../../../JGTL/include"
  "../Hypercube_NEAT/../../../zlib"
  "../../../../VoxSim/VoxBotCreator"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
