//
// Created by idgresearchp on 22/02/18.
//

#include "HCUBE_Defines.h"

#include "Experiments/HCUBE_XorExperimentMapElites.h"

using namespace NEAT;

namespace HCUBE
{
    XorExperimentMapElites::XorExperimentMapElites(string _experimentName,int _threadID)
            : Experiment(_experimentName,_threadID)
    {}

    void XorExperimentMapElites::processGroup(std::shared_ptr<NEAT::GeneticGeneration> generation)
    {

        for (int z = 0; z < group.size(); ++z) {

            NEAT::FastNetwork<float> network = group[z]->spawnFastPhenotypeStack<float>();

            NEAT::GeneticIndividualBehavior behaviorSignature;

            cv::Mat tmpSignature = cv::Mat::zeros(2, 1, CV_32F);
            behaviorAtype tmp;
            tmp.mat = tmpSignature;

            group[z]->reward(10);

            for (int x1 = 0; x1 < 2; x1++) {
                for (int x2 = 0; x2 < 2; x2++) {
                    network.reinitialize();

                    network.setValue("X1", x1);
                    network.setValue("X2", x2);
                    network.setValue("Bias", 0.3f);

                    network.update();

                    double value = network.getValue("Output");

                    double expectedValue = (double) (x1 ^ x2);

                    group[z]->reward(5000 * (2 - fabs(value - expectedValue)));

                    if (value == 0 )
                    {
                        tmpSignature.at<float>(0, 0) += 1;

                    } else{

                        tmpSignature.at<float>(1, 0) += 1;

                    }
                }
            }

            tmpSignature.at<float>(0, 0) /= 4;
            tmpSignature.at<float>(1, 0) /= 4;

            tmp.type = CV_32F;
            behaviorSignature.signatureVector.push_back(tmp);
            behaviorSignature.signature = &behaviorSignature.signatureVector[0];

            group[z]->individualID = "";

            // set behavior
            group[z]->behavior = behaviorSignature;
        }
    }

    void XorExperimentMapElites::processIndividualPostHoc(std::shared_ptr<NEAT::GeneticIndividual> individual)
    {
        NEAT::FastNetwork<float> network = individual->spawnFastPhenotypeStack<float>();

        //TODO Put in userdata

        double fitness = 10.0;
        double maxFitness = 10.0;

        for (int x1=0;x1<2;x1++)
        {
            for (int x2=0;x2<2;x2++)
            {
                network.reinitialize();

                network.setValue("X1",x1);
                network.setValue("X2",x2);
                network.setValue("Bias",0.3f);

                network.update();

                double value = network.getValue("Output");

                double expectedValue = (double)(x1 ^ x2);

                fitness += (5000*(2-fabs(value-expectedValue)));
                maxFitness += 5000*2;
            }
        }

        cout_ << "POST HOC ANALYSIS: " << fitness << "/" << maxFitness << endl;
    }

    Experiment* XorExperimentMapElites::clone()
    {
        XorExperimentMapElites* experiment = new XorExperimentMapElites(*this);

        return experiment;
    }
}


