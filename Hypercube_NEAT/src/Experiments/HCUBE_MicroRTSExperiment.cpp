//
// Created by idgresearchp on 13/02/18.
//

#include "Experiments/HCUBE_MicroRTSExperiment.h"

namespace HCUBE {

    MicroRTSExperiment::MicroRTSExperiment(string _experimentName, int _threadID) :
            Experiment(_experimentName,_threadID)
    {
        bestFit = 0.0;
        num_x_map = uint(NEAT::Globals::getSingleton()->getParameterValue("NumMapX"));
        num_y_map = uint(NEAT::Globals::getSingleton()->getParameterValue("NumMapY"));
        num_materials = uint(NEAT::Globals::getSingleton()->getParameterValue("MaterialsNum"));
    }

    NEAT::GeneticPopulation *MicroRTSExperiment::createInitialPopulation(int populationSize) {

        NEAT::GeneticPopulation* population = new NEAT::GeneticPopulation();

        return population;

    }

    void MicroRTSExperiment::processGroup(std::shared_ptr<NEAT::GeneticGeneration> generation) {
        int genNum = generation->getGenerationNumber() + 1;
        char buffer2[50];
        sprintf(buffer2, "%04i", genNum);

        for (int z = 0; z < group.size(); ++z) {
            std::ostringstream mkGenDir;
            if (genNum % (int) NEAT::Globals::getSingleton()->getParameterValue("RecordEntireGenEvery") == 0) {
                mkGenDir << "mkdir -p " << NEAT::Globals::getSingleton()->getRootFilename() << "Gen_" << buffer2;
            }
            std::system(mkGenDir.str().c_str());

            std::shared_ptr<NEAT::GeneticIndividual> individual = group[z];
            char buffer1[50];
            sprintf(buffer1, "%04i", genNum);
            std::ostringstream tmp;
            tmp << "Map" << "--Gen_" << buffer1 << "--Ind_" << individual;
            string individualID = tmp.str();
            cout_ << individualID << endl;
            processEvaluation(individual, individualID, genNum, bestFit);
        }
    }

    void MicroRTSExperiment::processEvaluation(std::shared_ptr<NEAT::GeneticIndividual> individual, string individualID,
                                               int genNum, double &bestFit) {
        double fitness;
        bool validIndividual;
        //initializes continuous space array with zeros. +1 is because we need to sample
        // these points at all corners of each voxel, leading to n+1 points in any dimension

        vector<vector<int>> map(num_x_map, vector<int>(num_y_map));

        vector<vector<int>> up_sampled_map(int(num_x_map*1), vector<int>(int(num_y_map*1)));


        // CPPN network for the individual
        NEAT::FastNetwork<double> network = individual->spawnFastPhenotypeStack<double>();
        double xNor, yNor;
        double has_material = 0.0;

        for (int i = 0; i < this->num_y_map*1; ++i) {
            for (int j = 0; j < this->num_x_map*1; ++j) {

                //get x, y coordinates and ask CPPN, there is something?
                //TODO we should not ask for the coordinates where we have the bases
                xNor = mapXYvalToNormalizedGridCoord(i, num_x_map*1);
                yNor = mapXYvalToNormalizedGridCoord(j, num_y_map*1);
                // reset cppn
                network.reinitialize();
                // set normalized values to inputs
                network.setValue("x", xNor);
                network.setValue("y", yNor);
                network.setValue("Bias", 0.3);

                if(bool(NEAT::Globals::getSingleton()->getParameterValue("SymmetryInputs")))
                {
                    network.setValue("d",sqrt(pow(xNor, 2) + pow(yNor, 2)));

//                    network.setValue("d_first_p",sqrt(pow(xNor - 1, 2) + pow(yNor - 1, 2)));
//
//                    network.setValue("d_second_p",sqrt(pow(xNor + 1, 2) + pow(yNor + 1, 2)));
                }

                // update the network after initializing the inputs
                network.update();
                // get value for voxel empty or not
//                has_material = network.getValue("Output");

                if (true) {
                    // get values for materials
                    double max_value = 0.0f;
                    double value = 0.0f;
                    int index_max_value = 0;

                    for (int matIndex = 0; matIndex < this->num_materials; ++matIndex) {
                        std::stringstream ss;
                        ss << matIndex;
                        value = network.getValue("OutputMat" + ss.str());

                        if (value > max_value) {
                            max_value = value;
                            index_max_value = matIndex;
                        }

                    }

                    // index 0 is for no materials
                    up_sampled_map[i][j] = index_max_value;
                }
                else
                {
                    up_sampled_map[i][j] = 0;
                }
            }
        }

        for (int i = 0; i < this->num_y_map; i += 1) {
            for (int j = 0; j < this->num_x_map; j += 1) {

                std::vector<int> counters = {0, 0 ,0, 0};

                for (int x = i; x < i + 1; x += 1) {
                    for (int y = j; y < j + 1; y += 1) {
                        counters[up_sampled_map[x][y]] += 1;
                    }
                }

                map[int(i/1)][int(j/1)] = std::distance(counters.begin(), std::max_element(counters.begin(), counters.end()));
            }
        }


        //fix map

        map[0][0] = 4;
        map[this->num_x_map - 1][this->num_y_map - 1] = 4;


        map[int(NEAT::Globals::getSingleton()->getParameterValue("barracks_x1"))][int(NEAT::Globals::getSingleton()->getParameterValue("barracks_y1"))] = 3;
        map[int(NEAT::Globals::getSingleton()->getParameterValue("barracks_x2"))][int(NEAT::Globals::getSingleton()->getParameterValue("barracks_y2"))] = 3;

//        //TODO DEBUG: to remove
//        for (int i = 0; i < this->num_y_map; ++i) {
//            for (int j = 0; j < this->num_x_map; ++j) {
//
//                cout << map[i][j] << " ";
//
//            }
//            cout << endl;
//        }


        // Write the vox simulation file given the individual

        writeMap(individual, individualID, map);
//
//        // Open md5sum file to create a key for the map with the saved fitness
//        string md5sumString = ReadMd5sum();
//        if(progressSimulationInfo) cout_ << "md5sum: " << md5sumString << "\n";

        // Read behavior variables, only if there is behavior
        NEAT::GeneticIndividualBehavior behaviorSignature;

        readBeaviour("", validIndividual, fitness, behaviorSignature, map);

        // set individual fitness
        individual->setFitness(fitness);

        // set behavior
        individual->behavior = behaviorSignature;

        individual->individualID = individualID;

        std::ostringstream moveFitFileCmd;
        char buffer2[50];
        sprintf(buffer2, "%04i", genNum);

        moveFitFileCmd << "mv " << "Map" << "--Gen_" << buffer2 << "--Ind_" << individual << ".xml";
        moveFitFileCmd << " "<< NEAT::Globals::getSingleton()->getRootFilename() <<"Gen_" << buffer2 << "/";
        std::system(moveFitFileCmd.str().c_str());
    }

    double MicroRTSExperiment::mapXYvalToNormalizedGridCoord(const int &r_xyVal, const int &r_numVoxelsXorY) {
        // turn the xth or yth node into its coordinates on a grid from -1 to 1, e.g. x values (1,2,3,4,5) become (-1, -.5 , 0, .5, 1)
        // this works with even numbers, and for x or y grids only 1 wide/tall, which was not the case for the original
        // e.g. see findCluster for the orignal versions where it was not a funciton and did not work with odd or 1 tall/wide
        if (r_numVoxelsXorY == 1)
            return 0;
        return (-1 + (r_xyVal * 2.0 / (r_numVoxelsXorY - 1)));
    }

    void MicroRTSExperiment::writeMap(std::shared_ptr<GeneticIndividual> ind, std::string individualID, vector<vector<int>>& map)
    {

//        ofstream md5file;
//        md5file.open ("md5sumTMP.txt");
        std::ostringstream myFileName;
        myFileName << individualID << ".xml";
        vector<int> rts_map = createArrayForMicroRTS(map);

        writeMicroRTSSimulation(myFileName.str(), rts_map, map);

//        for (int i = 0; i < voxbot.size(); ++i)
//            md5file << voxbot.at(i);
//        md5file.close();

    }

    vector<int> MicroRTSExperiment::createArrayForMicroRTS(vector<vector<int>>& map)
    {
        vector<int> flatten_map(this->num_y_map*this->num_x_map, 0);

        for (int i = 0; i < this->num_x_map; ++i) {
            for (int j = 0; j < this->num_y_map; ++j) {

                if (map[j][i] == 1)
                    flatten_map[i + j*this->num_y_map] = map[j][i];
                else
                    flatten_map[i + j*this->num_y_map] = 0;
            }
        }

        return flatten_map;

    }

    void MicroRTSExperiment::writeMicroRTSSimulation(std::string fileName, vector<int> &map_flattened, vector<vector<int>>& map)
    {

        std::ofstream m_OutFile;

        m_OutFile.open(fileName, std::ios::out);

        ostringstream  map_ascii;

        map_ascii << "<rts.PhysicalGameState width=\"" << this->num_x_map << "\" height=\""<< this->num_y_map <<"\">" << endl;

        map_ascii <<  "<terrain>";

        for(int i = 0; i < map_flattened.size(); i++)
        {
            map_ascii << map_flattened[i];
        }

        map_ascii << "</terrain>" << endl;

        map_ascii << "<players>" << endl
                  << "<rts.Player ID=\"0\" resources=\"5\"> " << endl
                  << "</rts.Player> "<< endl
                  << "<rts.Player ID=\"1\" resources=\"5\"> " << endl
                  << "</rts.Player>" << endl
                  << "</players>" << endl;

        map_ascii << "<units> " << endl;

        int id = 0;

        for (int i = 0; i < this->num_x_map; ++i) {
            for (int j = 0; j < this->num_y_map; ++j) {

                if(map[j][i] == 2) {
                    map_ascii << "<rts.units.Unit type=\"Resource\" ID=\"" << id
                              << "\" player=\"-1\" x=\""<< i <<"\" y=\""<< j <<"\" resources=\"20\" hitpoints=\"1\" > " << endl
                              << "</rts.units.Unit>" << endl;
                    id++;
                }
            }
        }

//        int player_id = -1;
//
//        for (int i = 0; i < this->num_x_map; ++i) {
//            for (int j = 0; j < this->num_y_map; ++j) {
//
//                if(map[j][i] == 3) {
//
//                    if(i*i + j*j < (i-7)*(i-7) + (j-7)*(j-7))
//                    {
//                        player_id = 0;
//                    }
//                    else{
//                        player_id = 1;
//                    }
//
//                    map_ascii << "<rts.units.Unit type=\"Barracks\" ID=\"" << id
//                              << "\" player=\""<< player_id<< "\" x=\""<< i <<"\" y=\""<< j <<"\" resources=\"0\" hitpoints=\"4\" > " << endl
//                              << "</rts.units.Unit>" << endl;
//                    id++;
//                }
//            }
//        }

        map_ascii << "<rts.units.Unit type=\"Barracks\" ID=\"" << ++id
                  << "\" player=\""<< 0<< "\" x=\""<< int(NEAT::Globals::getSingleton()->getParameterValue("barracks_y1")) <<"\" y=\""<< int(NEAT::Globals::getSingleton()->getParameterValue("barracks_x1")) <<"\" resources=\"0\" hitpoints=\"4\" > " << endl
                  << "</rts.units.Unit>" << endl;

        map_ascii << "<rts.units.Unit type=\"Barracks\" ID=\"" << ++id
                  << "\" player=\""<< 1<< "\" x=\""<< int(NEAT::Globals::getSingleton()->getParameterValue("barracks_y2")) <<"\" y=\""<< int(NEAT::Globals::getSingleton()->getParameterValue("barracks_x2")) <<"\" resources=\"0\" hitpoints=\"4\" > " << endl
                  << "</rts.units.Unit>" << endl;




        map_ascii << "<rts.units.Unit type=\"Base\" ID=\"0\" player=\"0\" x=\"0\" y=\"0\" resources=\"0\" hitpoints=\"10\" > " << endl
        <<   "</rts.units.Unit>" << endl
        << "<rts.units.Unit type=\"Base\" ID=\"1\" player=\"1\" x=\""<< this->num_x_map - 1 <<"\" y=\""<< this->num_y_map - 1 <<"\" resources=\"0\" hitpoints=\"10\" >" << endl
        << "</rts.units.Unit> " << endl
        << "</units>" << endl;

        map_ascii << "</rts.PhysicalGameState>" << endl;

        m_OutFile << map_ascii.str() << std::endl;

        m_OutFile.close();

        std::ostringstream simCmd;
        simCmd << "java -jar /home/idgresearch/Desktop/microRTS/out/artifacts/microRTS_jar/microRTS.jar " <<
                "\"" << fileName << "\"";

        cout << simCmd.str().c_str() << endl;

        std::system(simCmd.str().c_str());

    }

    void MicroRTSExperiment::readBeaviour(std::string filename, bool &valid, double &fitness, NEAT::GeneticIndividualBehavior &behavior,
                                          vector<vector<int>>& map) {
        //TODO test

        valid = true;
        cv::Mat tmpSignature = cv::Mat::zeros(2, 1, CV_32F);
        behaviorAtype tmp;
        tmp.mat = tmpSignature;


        float sim_time = 0;

        float max_sim_time = 1000;

        float units_produced = 0;

        float max_units_produced = 30;

        float entropy = 0;
        float max_entropy = 1.0f;

        std::ifstream m_InFile;

        m_InFile.open("results.txt", std::ios::in);

        string line;

        std::vector<float> sim_results;

        std::vector<float> feasibles, winners, sim_times, units, entropies;

        while ( getline (m_InFile,line) )
        {
            if (!line.empty())
                sim_results.push_back(std::stod(line));
        }


        for(int index = 0; index < 10; ++index) {

            cout << "winners " << sim_results[index * 6 + 1] << endl;

            feasibles.emplace_back(sim_results[index * 6]);
            winners.emplace_back(sim_results[index * 6 + 1]);
            sim_times.emplace_back(sim_results[index * 6 + 2]);
            units.emplace_back(sim_results[index * 6 + 3]);
            entropies.emplace_back(sim_results[index * 6 + 4]);
//            entropies.emplace_back(sim_results[index * 6 + 5]);
        }

        sim_time = std::accumulate(sim_times.begin(), sim_times.end(), 0.0f)/10.0f;

        units_produced = std::accumulate(units.begin(), units.end(), 0.0f)/10.0f;

        entropy = std::accumulate(entropies.begin(), entropies.end(), 0.0f)/10.0f;

        m_InFile.close();

        float total, first_player, second_player;

        total = 0;
        first_player = 0;
        second_player = 0;

        if(feasibles[0] > 1.29){
            for(int index = 0; index < winners.size(); ++index)
            {
                if(winners[index] != -1.0f)
                {
                    if(winners[index] == 0)
                    {
                        first_player += 1;
                    } else{
                        second_player += 1;
                    }

                    total += 1;
                    cout << winners[index] << endl;
                }
            }

            fitness = 1.3f;

            float balance = 0;

            if(total >= 0) {
                if (first_player != 0)
                    balance -= first_player / total * log2(first_player / total);

                if (second_player != 0)
                    balance -= second_player / total * log2(second_player / total);
            }

            cout << "balance " <<  balance << endl;

            fitness += balance;

        } else{
            fitness = feasibles[0];
        }


        float first = 0, second = 0, max_first = 0, max_second = 0;


        for (int i = 0; i < this->num_y_map; ++i) {
            for (int j = 0; j < this->num_x_map; ++j) {

                cout << map[i][j] << " ";

            }
            cout << endl;
        }

        //compute number of filled

        int number_of_filled = 0;
        int max_number_of_filled = 0;

        for (size_t i = 0; i < this->num_x_map; ++i) {
            for (size_t j = 0; j < this->num_y_map; ++j) {


                if (map[i][j] != 0)
                    number_of_filled++;

                max_number_of_filled++;


            }
        }

        max_number_of_filled -= 4;
        number_of_filled -= 4;

        int number_of_resources = 0;
        int max_number_of_resources = 0;

        //compute number of walls
        for (size_t i = 0; i < this->num_x_map; ++i) {
            for (size_t j = 0; j < this->num_y_map; ++j) {


                if (map[i][j] == 2)
                    number_of_resources++;

                max_number_of_resources++;


            }
        }

        max_number_of_resources -= 4;

        //max_number_of_resources = number_of_filled;


        //compute diagonal symmetry

        int diagonal_symmetry = 0;
        int max_diagonal_symmetry = 0;

        for (size_t i = 0; i < this->num_x_map - 1; ++i) {
            for (size_t j = 0; j < this->num_y_map - 1 - i; ++j) {

                if (map[i][j] == map[this->num_y_map - 1 - j][this->num_x_map - 1 - i]) {
                    diagonal_symmetry++;
                    //debug
//                        cout << i << "  " << j << "  " << this->num_y_map - 1 - j <<  "  "<< this->num_x_map - 1 - i << endl;
//                        cout << map[i][j] << " " << map[this->num_y_map - 1 - j][this->num_x_map - 1 - i] << endl;
                }

                max_diagonal_symmetry++;

            }
        }

        max_diagonal_symmetry -= 2;
        diagonal_symmetry -= 2;

        //compute central symmetry

//        for (size_t i = 0; i < this->num_x_map; ++i) {
//            for (size_t j = 0; j < int(this->num_y_map/2); ++j) {
//
//                if (map[i][j] == map[this->num_x_map - 1 - i][this->num_y_map - 1 - j]) {
//                    second++;
//                    //debug
////                        cout << i << "  " << j << "  " << this->num_y_map - 1 - j <<  "  "<< this->num_x_map - 1 - i << endl;
////                        cout << map[i][j] << " " << map[this->num_y_map - 1 - j][this->num_x_map - 1 - i] << endl;
//                }
//
//                max_second++;
//
//            }
//        }

//        max_second -= 2;
//        second -= 2;

//        vector<vector<int>> map_1(0, vector<int>(0));
//        map_1.push_back(vector<int>() ={0,0,0,0,0,0,0,0});
//        map_1.push_back(vector<int>() ={1,1,1,1,1,1,0,2});
//        map_1.push_back(vector<int>() ={0,0,0,0,0,0,0,0});
//        map_1.push_back(vector<int>() ={0,0,0,0,0,0,0,0});
//        map_1.push_back(vector<int>() ={1,1,1,1,1,1,0,2});
//        map_1.push_back(vector<int>() ={0,0,0,0,0,0,0,0});
//        map_1.push_back(vector<int>() ={0,1,1,1,1,1,1,0});
//        map_1.push_back(vector<int>() ={0,0,0,0,0,0,0,0});
//
//        vector<vector<int>> map_2(0, vector<int>(0));
//        map_2.push_back(vector<int>() ={0,1,0,2,0,0,0,0});
//        map_2.push_back(vector<int>() ={0,1,0,0,0,0,1,0});
//        map_2.push_back(vector<int>() ={0,1,0,0,0,1,0,0});
//        map_2.push_back(vector<int>() ={0,1,0,0,1,0,0,0});
//        map_2.push_back(vector<int>() ={0,0,0,1,0,0,1,0});
//        map_2.push_back(vector<int>() ={0,0,1,0,0,0,1,0});
//        map_2.push_back(vector<int>() ={0,1,0,0,0,0,1,0});
//        map_2.push_back(vector<int>() ={0,0,0,0,2,0,1,0});

//        for (size_t i = 0; i < this->num_x_map; ++i){
//            for (size_t j = 0; j < this->num_y_map; ++j) {
//                if(map_1[i][j] == map[i][j]) {
//                    first++;
//                }
//
//                if(map_2[i][j] == map[i][j]) {
//                    second++;
//                }
//            }
//        }
//
//        max_second = this->num_x_map*this->num_y_map - 2;
//        max_first = this->num_x_map*this->num_y_map - 2;
//
//        first -= 2;
//        second -= 2;

        /////compute branching factor

        std::vector<float> branching_factor_max;
        std::vector<float> branching_factor;

        for (size_t i = 0; i < this->num_x_map; ++i) {
            for (size_t j = 0; j < int(this->num_y_map); ++j) {

                int max_branch = 0;
                int branch = 0;

                for(int x = -1; x <= 1; x += 2)
                {
                    if(i + x >= 0 && i + x < this->num_x_map)
                    {
                        max_branch += 1;

                        if(map[i][j] == 0 && map[i+x][j] == 0)
                        {
                            branch += 1;
                        }

                    }

                }

                for(int y = -1; y <= 1; y += 2)
                {
                    if(j + y >= 0 && j + y < this->num_x_map)
                    {
                        max_branch += 1;

                        if(map[i][j] == 0 && map[i][j+y] == 0)
                        {
                            branch += 1;
                        }
                    }

                }


                branching_factor_max.push_back(max_branch);

                branching_factor.push_back(branch);


            }
        }

        float avg_branching_max = accumulate( branching_factor_max.begin(), branching_factor_max.end(), 0.0)/branching_factor_max.size();

        float avg_branching = accumulate( branching_factor.begin(), branching_factor.end(), 0.0)/branching_factor.size();

        if(units_produced > max_units_produced)
        {
            units_produced = max_units_produced;
        }


        first = diagonal_symmetry;
        max_first = max_diagonal_symmetry;

        second = number_of_filled;
        max_second = max_number_of_filled;


        cout << first << " " << max_first << " " << second << " " << max_second << endl;

        tmpSignature.at<float>(0, 0) = first/max_first;
        tmpSignature.at<float>(1, 0) = second/max_second;

        cout << first/max_first << " " << second/max_second << endl;

        cout << "fitness: " << fitness <<  endl;

        tmp.type = CV_32F;
        behavior.signatureVector.push_back(tmp);
        behavior.signature = &behavior.signatureVector[0];
    }

    Experiment* MicroRTSExperiment::clone()
    {
        MicroRTSExperiment* experiment = new MicroRTSExperiment(*this);
        return experiment;
    }
}
