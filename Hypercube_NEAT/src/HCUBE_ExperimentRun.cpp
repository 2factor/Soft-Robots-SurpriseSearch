#include "HCUBE_Defines.h"

#include "HCUBE_ExperimentRun.h"

#include "Experiments/HCUBE_Experiment.h"
#include "Experiments/HCUBE_XorExperiment.h"
#include "Experiments/HCUBE_VoxbotsExperiment.h"
#include "Experiments/HCUBE_MicroRTSExperiment.h"
#include "Experiments/HCUBE_MicroRTSExperiment2.h"
#include "Experiments/HCUBE_XorExperimentMapElites.h"
#include <thread>
#include <mutex>
#include <NEAT_MapElites.h>
#include <chrono>

#ifndef HCUBE_NOGUI
#include "HCUBE_MainFrame.h"
#include "HCUBE_UserEvaluationFrame.h"

#include "HCUBE_ExperimentPanel.h"
#endif

#include "HCUBE_EvaluationSet.h"

using namespace std::chrono;

#define _DEBUG

namespace HCUBE {
    ExperimentRun::ExperimentRun()
            :
            running(false),
            started(false),
            cleanup(false),
            populationMutex(new boost::mutex()),
            frame(NULL) {
    }

    ExperimentRun::~ExperimentRun() {
        delete populationMutex;
    }

    void ExperimentRun::setupExperiment(
            int _experimentType,
            string _outputFileName
    ) {
        experimentType = _experimentType;
        outputFileName = _outputFileName;

        cout_ << "SETTING UP EXPERIMENT TYPE: " << experimentType << endl;

        for (int a = 0; a < NUM_THREADS; a++) {
            switch (experimentType) {
                case EXPERIMENT_XOR:
                    experiments.push_back(std::shared_ptr<Experiment>(new XorExperiment("", a)));
                    break;
                case EXPERIMENT_SOFTBOT_CPPN_NEAT:
                    experiments.push_back(std::shared_ptr<Experiment>(new SoftBotExperiment("", a)));
                    break;
                case EXPERIMENT_MICRORTS_CPPN_NEAT:
                    experiments.push_back(std::shared_ptr<Experiment>(new MicroRTSExperiment("", a)));
                    break;
                case EXPERIMENT_MICRORTS_DIRECT:
                    experiments.push_back(std::shared_ptr<Experiment>(new MicroRTSExperiment2("", a)));
                    break;
                case EXPERIMENT_XOR_CPPN_NEAT:
                    experiments.push_back(std::shared_ptr<Experiment>(new XorExperiment("", a)));
                    break;
                default:
                    cout_ << string("ERROR: Unknown Experiment Type!\n");
                    throw CREATE_LOCATEDEXCEPTION_INFO("ERROR: Unknown Experiment Type!");
            }

        }

    }

    void ExperimentRun::createPopulation(string populationString) {
        if (iequals(populationString, "")) {
            int popSize = (int) NEAT::Globals::getSingleton()->getParameterValue("PopulationSize");
            population = std::shared_ptr<NEAT::GeneticPopulation>(
                    experiments[0]->createInitialPopulation(popSize)
            );
        } else {
#ifdef EPLEX_INTERNAL
            try {
                if (dynamic_cast<NEAT::CoEvoExperiment *>(experiments[0].get())) {
                    population = std::shared_ptr<NEAT::GeneticPopulation>(
                            new NEAT::GeneticPopulation(
                                    populationString,
                                    std::shared_ptr<NEAT::CoEvoExperiment>(
                                            (NEAT::CoEvoExperiment *) experiments[0]->clone())
                            )
                    );
                    return;
                }
            }
            catch (const std::exception &ex) {
                throw CREATE_LOCATEDEXCEPTION_INFO(string("EXCEPTION ON DYNAMIC CAST: ") + string(ex.what()));
            }
#endif

            population = std::shared_ptr<NEAT::GeneticPopulation>(
                    new NEAT::GeneticPopulation(populationString)
            );
        }
    }

    void ExperimentRun::setupExperimentInProgress(
            string populationFileName,
            string _outputFileName
    ) {
        outputFileName = _outputFileName;

        {
            TiXmlDocument doc(populationFileName);

            bool loadStatus;

            if (iends_with(populationFileName, ".gz")) {
                loadStatus = doc.LoadFileGZ();
            } else {
                loadStatus = doc.LoadFile();
            }

            if (!loadStatus) {
                throw CREATE_LOCATEDEXCEPTION_INFO("Error trying to load the XML file!");
            }

            TiXmlElement *element = doc.FirstChildElement();

            NEAT::Globals *globals = NEAT::Globals::init(element);

            //Destroy the document
        }

        int experimentType = int(NEAT::Globals::getSingleton()->getParameterValue("ExperimentType") + 0.001);

        cout_ << "Loading Experiment: " << experimentType << endl;

        setupExperiment(experimentType, _outputFileName);

        cout_ << "Experiment set up.  Creating population...\n";

        createPopulation(populationFileName);

        cout_ << "Population Created\n";
    }

    void ExperimentRun::start() {
        cout << "Experiment started\n";

        auto start = high_resolution_clock::now();


#ifndef _DEBUG
        try
        {
#endif

        int maxGenerations = int(NEAT::Globals::getSingleton()->getParameterValue("MaxGenerations"));

        started = running = true;

        int firstGen = (population->getGenerationCount() - 1);
        for (int generations = firstGen; generations < maxGenerations; generations++) {
            if (generations > firstGen) {
                //Even if we are loading an existing population,
                //Re-evaluate all of the individuals
                //as a sanity check
                boost::mutex::scoped_lock scoped_lock(*populationMutex);
                cout << "PRODUCING NEXT GENERATION\n";
                produceNextGeneration();
                cout << "DONE PRODUCING\n";
            }

            if (experiments[0]->performUserEvaluations()) {
#ifdef HCUBE_NOGUI
                throw CREATE_LOCATEDEXCEPTION_INFO("ERROR: TRIED TO USE INTERACTIVE EVOLUTION WITH NO GUI!");
#else
                frame->getUserEvaluationFrame()->updateEvaluationPanels();
                running=false;
                while (!running)
                {
                    boost::xtime xt;
#if BOOST_VERSION > 105000
                    boost::xtime_get(&xt, boost::TIME_UTC_);
#else
                    boost::xtime_get(&xt, boost::TIME_UTC);
#endif
                    xt.sec += 1;
                    boost::thread::sleep(xt); // Sleep for 1/2 second
                    //cout_ << "Sleeping while user evaluates!\n";
                }
#endif
            } else {
                while (!running) {
                    boost::xtime xt;
#if BOOST_VERSION > 105000
                    boost::xtime_get(&xt, boost::TIME_UTC_);
#else
                    boost::xtime_get(&xt, boost::TIME_UTC);
#endif
                    xt.sec += 1;
                    boost::thread::sleep(xt); // Sleep for 1/2 second
                }
                preprocessPopulation();
                evaluatePopulation();
            }

            cout << "Finishing evaluations\n";
            finishEvaluations();
            cout << "Evaluations Finished\n";
        }
        cout << "Experiment finished\n";

        cout << "Saving best individuals...";
        string bestFileName = NEAT::Globals::getSingleton()->getRootFilename() +
                              outputFileName.substr(0, outputFileName.length() - 4) + string("_best.xml");
        population->dumpBest(bestFileName, true, true);

        cout << endl;

        cout << "Save time experiment" <<  endl;

        auto stop = high_resolution_clock::now();

        auto duration = duration_cast<seconds>(stop - start);

        ofstream timeFile(NEAT::Globals::getSingleton()->getRootFilename() + "time.txt",
                          std::ofstream::out);

        if (timeFile.is_open()) {
            timeFile << duration.count();
        }
        timeFile.close();


        //ostringstream moveBestIndividual;
        //moveBestIndividual << "mv " << bestFileName.c_str() << " " << NEAT::Globals::getSingleton()->getRootFilename();
        //std::system(moveBestIndividual.str().c_str());

        cout << "Done!\n";

        cout << "Deleting backup file...";
        boost::filesystem::remove(outputFileName + string(".backup.xml.gz"));
        cout << "Done!\n";

#ifndef _DEBUG
        }
        catch (const std::exception &ex)
        {
            cout_ << "CAUGHT ERROR AT " << __FILE__ << " : " << __LINE__ << endl;
            CREATE_PAUSE(ex.what());
        }
        catch (...)
        {
            cout_ << "CAUGHT ERROR AT " << __FILE__ << " : " << __LINE__ << endl;
            CREATE_PAUSE("AN UNKNOWN EXCEPTION HAS BEEN THROWN!");
        }
#endif
    }

    void ExperimentRun::start_multi() {
        cout << "Experiment MOEA started\n";

        auto start = high_resolution_clock::now();

        int maxGenerations = int(NEAT::Globals::getSingleton()->getParameterValue("MaxGenerations"));

        started = running = true;

        int firstGen = (population->getGenerationCount() - 1);

        for (int generations = firstGen; generations < maxGenerations; generations++) {
            if (generations > firstGen) {
                //Even if we are loading an existing population,
                //Re-evaluate all of the individuals
                //as a sanity check
                boost::mutex::scoped_lock scoped_lock(*populationMutex);
                cout << "PRODUCING NEXT GENERATION\n";

                population->produceNextGenerationMulti();

                cout << "DONE PRODUCING\n";
            }

            if (experiments[0]->performUserEvaluations()) {
#ifdef HCUBE_NOGUI
                throw CREATE_LOCATEDEXCEPTION_INFO("ERROR: TRIED TO USE INTERACTIVE EVOLUTION WITH NO GUI!");
#else
                frame->getUserEvaluationFrame()->updateEvaluationPanels();
                running=false;
                while (!running)
                {
                    boost::xtime xt;
#if BOOST_VERSION > 105000
                    boost::xtime_get(&xt, boost::TIME_UTC_);
#else
                    boost::xtime_get(&xt, boost::TIME_UTC);
#endif
                    xt.sec += 1;
                    boost::thread::sleep(xt); // Sleep for 1/2 second
                    //cout_ << "Sleeping while user evaluates!\n";
                }
#endif
            } else {
                while (!running) {
                    boost::xtime xt;
#if BOOST_VERSION > 105000
                    boost::xtime_get(&xt, boost::TIME_UTC_);
#else
                    boost::xtime_get(&xt, boost::TIME_UTC);
#endif
                    xt.sec += 1;
                    boost::thread::sleep(xt); // Sleep for 1/2 second
                }
                preprocessPopulation();
                evaluatePopulation();
            }

            cout << "Finishing evaluations\n";
            finishEvaluationsMultiObjective();
            cout << "Evaluations Finished\n";

        }
        cout << "Experiment finished\n";

        cout << "Save time experiment" <<  endl;

        auto stop = high_resolution_clock::now();

        auto duration = duration_cast<seconds>(stop - start);

        ofstream timeFile(NEAT::Globals::getSingleton()->getRootFilename() + "time.txt",
                          std::ofstream::out);

        if (timeFile.is_open()) {
            timeFile << duration.count();
        }
        timeFile.close();

        cout << "Saving best individuals...";
        string bestFileName = NEAT::Globals::getSingleton()->getRootFilename() +
                              outputFileName.substr(0, outputFileName.length() - 4) + string("_best.xml");
        population->dumpBest(bestFileName, true, true);

        //ostringstream moveBestIndividual;
        //moveBestIndividual << "mv " << bestFileName.c_str() << " " << NEAT::Globals::getSingleton()->getRootFilename();
        //std::system(moveBestIndividual.str().c_str());

        cout << "Done!\n";

        cout << "Deleting backup file...";
        boost::filesystem::remove(outputFileName + string(".backup.xml.gz"));
        cout << "Done!\n";

#ifndef _DEBUG
        }
    catch (const std::exception &ex)
    {
        cout_ << "CAUGHT ERROR AT " << __FILE__ << " : " << __LINE__ << endl;
        CREATE_PAUSE(ex.what());
    }
    catch (...)
    {
        cout_ << "CAUGHT ERROR AT " << __FILE__ << " : " << __LINE__ << endl;
        CREATE_PAUSE("AN UNKNOWN EXCEPTION HAS BEEN THROWN!");
    }
#endif
    }

    void ExperimentRun::start_map_elites() {

        MapElites map_elites;

        cout << "Saving random seed...";

        string filename = NEAT::Globals::getSingleton()->getRootFilename() +
                          outputFileName.substr(0, outputFileName.length() - 4) + string("_seed.xml");
        TiXmlDocument doc(filename);

        TiXmlElement *root = new TiXmlElement("Genetics");

        Globals::getSingleton()->dump(root);

        doc.LinkEndChild(root);

        doc.SaveFile();

        cout << "Experiment started\n";

        int maxGenerations = int(NEAT::Globals::getSingleton()->getParameterValue("MaxGenerations"));

        started = running = true;

        int firstGen = 0;
        for (int generations = firstGen; generations < maxGenerations; generations++) {

            cout << "generation " << generations << endl;

            if (generations > firstGen) {
                //Even if we are loading an existing population,
                //Re-evaluate all of the individuals
                //as a sanity check
                cout << "PRODUCING NEXT GENERATION\n";
                map_elites.epoch(generations);
                cout << "DONE PRODUCING\n";
            }

            //            vector<std::shared_ptr<NEAT::DirectRepresentation> >::iterator tmpIterator;
            //                std::shared_ptr<GeneticIndividual> tmp_ind_ptr(new GeneticIndividual(*tmpIterator));
            
            vector<std::shared_ptr<NEAT::GeneticIndividual> >::iterator tmpIterator;

            for (tmpIterator = map_elites.population.begin(); tmpIterator != map_elites.population.end(); ++tmpIterator) {
                experiments[0]->addIndividualToGroup(*tmpIterator);
            }

            std::shared_ptr<GeneticGeneration> tmp_gen_ptr(new GeneticGeneration(generations));

            experiments[0]->processGroup(tmp_gen_ptr);

            experiments[0]->clearGroup();

            //TODO find a way to get parents, but fine for now
            map_elites.update_archive();


            map_elites.generationData(generations);

            map_elites.printElites(generations);

        }

        map_elites.printElites(-1);

        if (bool(NEAT::Globals::getSingleton()->getParameterValue("StoreBestIndividuals"))) {
            if (!boost::filesystem::exists("BestIndividuals")) {
                boost::filesystem::path dir(NEAT::Globals::getSingleton()->getRootFilename() + "BestIndividuals");

                cout << NEAT::Globals::getSingleton()->getRootFilename() + "BestIndividuals" << endl;
                boost::filesystem::create_directory(dir);
            }
            cout << "Storing best individuals\n";

            for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1")); ++i)
            {
                for(int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2")); ++j)
                {

                    if(map_elites.getInd(i, j))
                    {
                        char buffer1[50];
                        char fit[100];


                        sprintf(fit, "%.8lf", map_elites.getInd(i, j)->getFitness());
                        std::ostringstream tmp;
                        tmp << "Softbot" << "--Fit_" << fit << "--Ind_"
                            << map_elites.getInd(i, j) << ".vxa";
                        string individualID = tmp.str();

                        int num_x_voxels = uint(NEAT::Globals::getSingleton()->getParameterValue("NumVoxelsX"));
                        int num_y_voxels = uint(NEAT::Globals::getSingleton()->getParameterValue("NumVoxelsY"));
                        int num_z_voxels = uint(NEAT::Globals::getSingleton()->getParameterValue("NumVoxelsZ"));

                        VoxBotCreator vxbc;
                        vxbc.LoadVXASimulation("../../../../SimFiles/default/default.vxa");
                        if (vxbc.getXDim() != num_x_voxels || vxbc.getYDim() != num_y_voxels || vxbc.getZDim() != num_z_voxels)
                            vxbc.resizeStructure(num_x_voxels, num_y_voxels, num_z_voxels);
                        vxbc.clearStructure();
                        vxbc.InsertVoxBot(map_elites.getInd(i, j)->VoxBotSimFile);
                        if (bool(NEAT::Globals::getSingleton()->getParameterValue("EvolveMaterialProperties"))) {
                            for (int matIndex = 0; matIndex < vxbc.GetNumMaterials(); ++matIndex) {
                                vxbc.setMatProperties(matIndex + 1, map_elites.getInd(i, j)->MaterialProperties[matIndex][0],
                                                      map_elites.getInd(i, j)->MaterialProperties[matIndex][1],
                                                      map_elites.getInd(i, j)->MaterialProperties[matIndex][2],
                                                      map_elites.getInd(i, j)->MaterialProperties[matIndex][3]);
                            }
                        }
                        vxbc.writeVXASimulation(individualID.c_str());

                        ostringstream moveBestIndividual;
                        moveBestIndividual << "mv " << individualID.c_str() << " "
                                           << NEAT::Globals::getSingleton()->getRootFilename() + "BestIndividuals/";
                        std::system(moveBestIndividual.str().c_str());

                    }



                }

            }
            cout << "Storing best individuals finished\n";
        }
    }

    void ExperimentRun::preprocessPopulation() {
        cout << "Preprocessing population\n";
        std::shared_ptr<NEAT::GeneticGeneration> generation = population->getGeneration();

        for (int a = 0; a < generation->getIndividualCount(); a++) {
            experiments[0]->preprocessIndividual(generation, generation->getIndividual(a));
        }
    }

    void ExperimentRun::evaluatePopulation() {
        std::shared_ptr<NEAT::GeneticGeneration> generation = population->getGeneration();
        //Randomize population order for evaluation
        generation->randomizeIndividualOrder();

        std::mutex mutex_eval;

        int populationSize = population->getIndividualCount();
        cout_ << "Processing group..." << endl;
        if (NUM_THREADS == 1) {
            //Bypass the threading logic for a single thread

            EvaluationSet evalSet(
                    experiments[0],
                    generation,
                    population->getIndividualIterator(0),
                    populationSize
            );
            evalSet.run();
        } else {
            int populationPerProcess = populationSize / NUM_THREADS;

            std::thread **threads = new std::thread *[NUM_THREADS];
            EvaluationSet **evaluationSets = new EvaluationSet *[NUM_THREADS];

            for (int i = 0; i < NUM_THREADS; ++i) {
                if (i + 1 == NUM_THREADS) {
                    //Fix for uneven distribution
                    int populationIteratorSize =
                            populationSize
                            - populationPerProcess * (NUM_THREADS - 1);

                    experiments[i]->mutex = &mutex_eval;

                    evaluationSets[i] =
                            new EvaluationSet(
                                    experiments[i],
                                    generation,
                                    population->getIndividualIterator(populationPerProcess * i),
                                    populationIteratorSize,
                                    &mutex_eval
                            );
                } else {
                    experiments[i]->mutex = &mutex_eval;

                    evaluationSets[i] =
                            new EvaluationSet(
                                    experiments[i],
                                    generation,
                                    population->getIndividualIterator(populationPerProcess * i),
                                    populationPerProcess,
                                    &mutex_eval
                            );
                }

                threads[i] =
                        new std::thread(
                                //boost::bind(
                                &EvaluationSet::run,
                                evaluationSets[i]
                                //  )
                        );
            }

            //loop through each thread, making sure it is finished before we move on
            for (int i = 0; i < NUM_THREADS; ++i) {
                /*if (!evaluationSets[i]->isFinished())
                      {
                      --i;
                      boost::xtime xt;
                      boost::xtime_get(&xt, boost::TIME_UTC);
                      xt.sec += 1;
                      boost::thread::sleep(xt); // Sleep for 1/2 second
                      }*/
                threads[i]->join();
            }

            for (int i = 0; i < NUM_THREADS; ++i) {
                delete threads[i];
                delete evaluationSets[i];
            }

            delete[] threads;
            delete[] evaluationSets;
        }
        cout_ << "Done processing group..." << endl;
    }

    void ExperimentRun::finishEvaluationsMultiObjective() {

        if (bool(NEAT::Globals::getSingleton()->getParameterValue("MOEA"))) {
            cout << "Multi Objectives ..." << endl;

            population->multiObjectiveComputation();

            cout << "Done" << endl;
        }

        if (bool(NEAT::Globals::getSingleton()->getParameterValue("StoreBestIndividuals"))) {
            if (!boost::filesystem::exists("BestIndividuals")) {
                boost::filesystem::path dir(NEAT::Globals::getSingleton()->getRootFilename() + "BestIndividuals");

                cout << NEAT::Globals::getSingleton()->getRootFilename() + "BestIndividuals" << endl;
                boost::filesystem::create_directory(dir);
            }
            cout << "Storing best individuals\n";
            std::shared_ptr<NEAT::GeneticGeneration> generation = population->getGeneration();
            std::shared_ptr<NEAT::GeneticIndividual> GenerationBestIndividual;

            GenerationBestIndividual = generation->getGenerationChampionFitnessBackUp();


            if (GenerationBestIndividual->isValid()) {
                char buffer1[100];
                sprintf(buffer1, "%04i", generation->getGenerationNumber());
                char fit[200];
                char orFit[200];


                sprintf(fit, "%.8lf", GenerationBestIndividual->getFitnessBackUp());

                sprintf(orFit, "%.8lf", GenerationBestIndividual->originalFitness);
                std::ostringstream tmp;
                tmp << "Softbot" << "--Fit_" << fit << "--orFit_" << orFit << "--Gen_" << buffer1 << "--Ind_"
                    << GenerationBestIndividual << ".vxa";
                string individualID = tmp.str();

                int num_x_voxels = uint(NEAT::Globals::getSingleton()->getParameterValue("NumVoxelsX"));
                int num_y_voxels = uint(NEAT::Globals::getSingleton()->getParameterValue("NumVoxelsY"));
                int num_z_voxels = uint(NEAT::Globals::getSingleton()->getParameterValue("NumVoxelsZ"));

                VoxBotCreator vxbc;
                vxbc.LoadVXASimulation("../../../../SimFiles/default/default.vxa");
                if (vxbc.getXDim() != num_x_voxels || vxbc.getYDim() != num_y_voxels || vxbc.getZDim() != num_z_voxels)
                    vxbc.resizeStructure(num_x_voxels, num_y_voxels, num_z_voxels);
                vxbc.clearStructure();
                vxbc.InsertVoxBot(GenerationBestIndividual->VoxBotSimFile);
                if (bool(NEAT::Globals::getSingleton()->getParameterValue("EvolveMaterialProperties"))) {
                    for (int matIndex = 0; matIndex < vxbc.GetNumMaterials(); ++matIndex) {
                        vxbc.setMatProperties(matIndex + 1, GenerationBestIndividual->MaterialProperties[matIndex][0],
                                              GenerationBestIndividual->MaterialProperties[matIndex][1],
                                              GenerationBestIndividual->MaterialProperties[matIndex][2],
                                              GenerationBestIndividual->MaterialProperties[matIndex][3]);
                    }
                }
                vxbc.writeVXASimulation(individualID.c_str());

                ostringstream moveBestIndividual;
                moveBestIndividual << "mv " << individualID.c_str() << " "
                                   << NEAT::Globals::getSingleton()->getRootFilename() + "BestIndividuals/";
                std::system(moveBestIndividual.str().c_str());
            }


            cout << "Storing best individuals finished\n";
        }

        // Clear vectors with structures
        std::shared_ptr<NEAT::GeneticGeneration> generation = population->getGeneration();
        for (int a = 0; a < generation->getIndividualCount(); a++) {
            generation->getIndividual(a)->VoxBotSimFile.clear();
        }

        cout << "Adjusting fitness multi...\n";
        // compute fronts, calcute crowding distance
        population->adjustFitnessMulti();
        cout << "done adjusting\n";

        // prints stats and writes them in a file
        population->generationData();

        if (cleanup)
            population->cleanupOld(INT_MAX / 2);

        cout << "Dumping best individuals...\n";
        population->dumpBest(outputFileName + string(".backup.xml"), true, true);
#ifndef HCUBE_NOGUI
        if (frame)
        {
            frame->updateNumGenerations(population->getGenerationCount());
        }
#endif

        cout << "Resetting generation data...\n";
        experiments[0]->resetGenerationData(generation);

        for (int a = 0; a < population->getIndividualCount(); a++) {
            //cout_ << __FILE__ << ":" << __LINE__ << endl;
            experiments[0]->addGenerationData(generation, population->getIndividual(a));
        }

    }

    void ExperimentRun::finishEvaluations() {
        if (bool(NEAT::Globals::getSingleton()->getParameterValue("NoveltySearch"))) {
            cout << "Novelty search..." << endl;
            population->noveltySearchComputation();
            cout << "Done" << endl;
        }

        if (bool(NEAT::Globals::getSingleton()->getParameterValue("SurpriseSearch"))) {
            cout << "Surprise Search..." << endl;
            population->surpriseSearchComputation();
            cout << "Done" << endl;
        }

        if (bool(NEAT::Globals::getSingleton()->getParameterValue("NoveltySurprise"))) {
            cout << "Novelty Surprise Search..." << endl;
            population->noveltySurpriseSearchComputation();
            cout << "Done" << endl;
        }


        if (bool(NEAT::Globals::getSingleton()->getParameterValue("FitnessNovelty")) &&
            !bool(NEAT::Globals::getSingleton()->getParameterValue("NoveltySearch"))) {
            cout << "Novel individuals for fitness based..." << endl;
            population->fitnessSearchNoveltyComputation();
            cout << "Done" << endl;
        }

        if (bool(NEAT::Globals::getSingleton()->getParameterValue("ComputeSparsity")) &&
            (bool(NEAT::Globals::getSingleton()->getParameterValue("FitnessNovelty")) ||
             bool(NEAT::Globals::getSingleton()->getParameterValue("NoveltySearch")))) {
            population->computeBehaviorSparsity();
        }

        // prints stats and writes them in a file
        population->generationData();

        if (bool(NEAT::Globals::getSingleton()->getParameterValue("NoveltySearch")) &&
            bool(NEAT::Globals::getSingleton()->getParameterValue("StoreNovelIndividuals"))) {
            if (!boost::filesystem::exists("NovelIndividuals")) {
                boost::filesystem::path dir("NovelIndividuals");
                boost::filesystem::create_directory(dir);
            }
            cout_ << "Storing novel individuals\n";
            std::shared_ptr<NEAT::GeneticGeneration> generation = population->getGeneration();
            for (int a = 0; a < generation->getIndividualCount(); a++) {
                std::shared_ptr<NEAT::GeneticIndividual> tmpInd = generation->getIndividual(a);
                std::vector<std::string> outNames;
                if (tmpInd->isNovel()) {
                    char buffer1[50];
                    sprintf(buffer1, "%04i", generation->getGenerationNumber());
                    char nov[100];
                    sprintf(nov, "%.8lf", tmpInd->getFitness());
                    char fit[100];
                    sprintf(fit, "%.8lf", tmpInd->getFitnessBackUp());
                    std::ostringstream tmp;
                    tmp << "Softbot" << "--Gen_" << buffer1 << "--Ind_" << tmpInd << "--Nov_" << nov << "--Fit_" << fit;
                    string individualID = tmp.str();
                    tmpInd->behavior.writeSignature(individualID, ".csv", false, outNames);
                    std::ostringstream moveSigFileCmd;
                    moveSigFileCmd << "mv " << individualID << ".csv " << "NovelIndividuals/";
                    std::system(moveSigFileCmd.str().c_str());
                }
            }
            cout_ << "Storing novel individuals finished\n";
        }

        if (bool(NEAT::Globals::getSingleton()->getParameterValue("FitnessNovelty")) &&
            bool(NEAT::Globals::getSingleton()->getParameterValue("FitnessStoreNovelIndividuals"))) {
            if (!boost::filesystem::exists("NovelIndividuals")) {
                boost::filesystem::path dir("NovelIndividuals");
                boost::filesystem::create_directory(dir);
            }
            cout_ << "Storing novel individuals\n";
            std::shared_ptr<NEAT::GeneticGeneration> generation = population->getGeneration();
            for (int a = 0; a < generation->getIndividualCount(); a++) {
                std::shared_ptr<NEAT::GeneticIndividual> tmpInd = generation->getIndividual(a);
                std::vector<std::string> outNames;
                if (tmpInd->isNovel()) {
                    char buffer1[50];
                    sprintf(buffer1, "%04i", generation->getGenerationNumber());
                    char fit[100];
                    sprintf(fit, "%.8lf", tmpInd->getFitness());
                    std::ostringstream tmp;
                    tmp << "Softbot" << "--Gen_" << buffer1 << "--Ind_" << tmpInd << "--Fit_" << fit;
                    string individualID = tmp.str();
                    tmpInd->behavior.writeSignature(individualID, ".csv", false, outNames);
                    std::ostringstream moveSigFileCmd;
                    moveSigFileCmd << "mv " << individualID << ".csv " << "NovelIndividuals/";
                    std::system(moveSigFileCmd.str().c_str());
                }
            }
            cout_ << "Storing novel individuals finished\n";
        }

        if (bool(NEAT::Globals::getSingleton()->getParameterValue("StoreBestIndividuals"))) {
            if (!boost::filesystem::exists("BestIndividuals")) {
                boost::filesystem::path dir(NEAT::Globals::getSingleton()->getRootFilename() + "BestIndividuals");

                cout << NEAT::Globals::getSingleton()->getRootFilename() + "BestIndividuals" << endl;
                boost::filesystem::create_directory(dir);
            }
            cout << "Storing best individuals\n";
            std::shared_ptr<NEAT::GeneticGeneration> generation = population->getGeneration();
            std::shared_ptr<NEAT::GeneticIndividual> GenerationBestIndividual;


            if (bool(NEAT::Globals::getSingleton()->getParameterValue("NoveltySearch"))
                ||
                bool(NEAT::Globals::getSingleton()->getParameterValue("SurpriseSearch"))
                ||
                bool(NEAT::Globals::getSingleton()->getParameterValue("NoveltySurprise")))

                GenerationBestIndividual = generation->getGenerationChampionFitnessBackUp();
            else
                GenerationBestIndividual = generation->getGenerationChampion();


            if (GenerationBestIndividual->isValid()) {
                char buffer1[50];
                sprintf(buffer1, "%04i", generation->getGenerationNumber());
                char fit[100];
                char orFit[100];

                if (bool(NEAT::Globals::getSingleton()->getParameterValue("NoveltySearch"))
                    ||
                    bool(NEAT::Globals::getSingleton()->getParameterValue("SurpriseSearch"))
                    ||
                    bool(NEAT::Globals::getSingleton()->getParameterValue("NoveltySurprise")))
                    sprintf(fit, "%.8lf", GenerationBestIndividual->getFitnessBackUp());
                else
                    sprintf(fit, "%.8lf", GenerationBestIndividual->getFitness());

                sprintf(orFit, "%.8lf", GenerationBestIndividual->originalFitness);
                std::ostringstream tmp;
                tmp << "Softbot" << "--Fit_" << fit << "--orFit_" << orFit << "--Gen_" << buffer1 << "--Ind_"
                    << GenerationBestIndividual << ".vxa";
                string individualID = tmp.str();

                int num_x_voxels = uint(NEAT::Globals::getSingleton()->getParameterValue("NumVoxelsX"));
                int num_y_voxels = uint(NEAT::Globals::getSingleton()->getParameterValue("NumVoxelsY"));
                int num_z_voxels = uint(NEAT::Globals::getSingleton()->getParameterValue("NumVoxelsZ"));

                VoxBotCreator vxbc;
                vxbc.LoadVXASimulation("../../../../SimFiles/default/default.vxa");
                if (vxbc.getXDim() != num_x_voxels || vxbc.getYDim() != num_y_voxels || vxbc.getZDim() != num_z_voxels)
                    vxbc.resizeStructure(num_x_voxels, num_y_voxels, num_z_voxels);
                vxbc.clearStructure();
                vxbc.InsertVoxBot(GenerationBestIndividual->VoxBotSimFile);
                if (bool(NEAT::Globals::getSingleton()->getParameterValue("EvolveMaterialProperties"))) {
                    for (int matIndex = 0; matIndex < vxbc.GetNumMaterials(); ++matIndex) {
                        vxbc.setMatProperties(matIndex + 1, GenerationBestIndividual->MaterialProperties[matIndex][0],
                                              GenerationBestIndividual->MaterialProperties[matIndex][1],
                                              GenerationBestIndividual->MaterialProperties[matIndex][2],
                                              GenerationBestIndividual->MaterialProperties[matIndex][3]);
                    }
                }
                vxbc.writeVXASimulation(individualID.c_str());

                ostringstream moveBestIndividual;
                moveBestIndividual << "mv " << individualID.c_str() << " "
                                   << NEAT::Globals::getSingleton()->getRootFilename() + "BestIndividuals/";
                std::system(moveBestIndividual.str().c_str());
            }


            cout << "Storing best individuals finished\n";
        }

        // Clear vectors with structures
        std::shared_ptr<NEAT::GeneticGeneration> generation = population->getGeneration();
        for (int a = 0; a < generation->getIndividualCount(); a++) {
            generation->getIndividual(a)->VoxBotSimFile.clear();
        }

        cout << "Adjusting fitness...\n";
        population->adjustFitness(); // Does not change individuals' fitnesses, only species fitnesses
        cout << "Cleaning up...\n";

        if (cleanup)
            population->cleanupOld(INT_MAX / 2);
        cout << "Dumping best individuals...\n";
        population->dumpBest(outputFileName + string(".backup.xml"), true, true);
#ifndef HCUBE_NOGUI
        if (frame)
        {
            frame->updateNumGenerations(population->getGenerationCount());
        }
#endif

        cout << "Resetting generation data...\n";
        experiments[0]->resetGenerationData(generation);

        for (int a = 0; a < population->getIndividualCount(); a++) {
            //cout_ << __FILE__ << ":" << __LINE__ << endl;
            experiments[0]->addGenerationData(generation, population->getIndividual(a));
        }
    }

    void ExperimentRun::produceNextGeneration() {
        cout << "Producing next generation.\n";
        try {
            population->produceNextGeneration();
        }
        catch (const std::exception &ex) {
            cout_ << "EXCEPTION DURING POPULATION REPRODUCTION: " << endl;
            CREATE_PAUSE(ex.what());
        }
        catch (...) {
            cout_ << "Unhandled Exception\n";
        }
    }

}
