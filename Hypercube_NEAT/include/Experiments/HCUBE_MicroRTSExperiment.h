//
// Created by idgresearchp on 13/02/18.
//

#ifndef NEAT_HCUBE_MICRORTSEXPERIMENT_H
#define NEAT_HCUBE_MICRORTSEXPERIMENT_H

#include "HCUBE_Experiment.h"
#include "Array3D.h"
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "NEAT_GeneticIndividualBehavior.h"
#include "HCUBE_Defines.h"
#include <string>
#include <iostream>
#include <ctime>

namespace HCUBE {

    class MicroRTSExperiment : public Experiment {

    protected:
        unsigned int num_x_map;
        unsigned int num_y_map;
        unsigned int num_materials;
        unsigned int numConnections;
        unsigned int fitnessType;
        unsigned int behaviorType;
        bool progressSimulationInfo;
        double behaviorRecordInterval;
        double behaviorRecordStartInterval;
        bool allBehaviors;
        double bestFit = 0.0f;
        string outDir;

        virtual NEAT::GeneticPopulation* createInitialPopulation(int populationSize);

        void processEvaluation(std::shared_ptr<NEAT::GeneticIndividual> individual, string individualID, int genNum, double &bestFit);

        double mapXYvalToNormalizedGridCoord(const int & r_xyVal, const int & r_numVoxelsXorY);

        vector<int> createArrayForMicroRTS(vector<vector<int>>& map);
        void writeMicroRTSSimulation(std::string fileName, vector<int> &map_flattened, vector<vector<int>>& map);
        void writeMap(std::shared_ptr<GeneticIndividual> ind, std::string individualID, vector<vector<int>>& map);

    public:

        MicroRTSExperiment(string _experimentName, int _threadID);


        virtual ~MicroRTSExperiment()
        {}

        virtual void processGroup(std::shared_ptr<NEAT::GeneticGeneration> generation);

        void readBeaviour(std::string filename, bool &valid, double &fitness, NEAT::GeneticIndividualBehavior &behavior, vector<vector<int>>& map);

        virtual Experiment* clone();

        virtual int getGroupCapacity()
        {
            return int(NEAT::Globals::getSingleton()->getParameterValue("PopulationSize"));
        }

    };
}


#endif //NEAT_HCUBE_MICRORTSEXPERIMENT_H
