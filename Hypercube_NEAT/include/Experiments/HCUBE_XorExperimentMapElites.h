//
// Created by idgresearchp on 22/02/18.
//

#ifndef NEAT_HCUBE_XOREXPERIMENTMAPELITES_H
#define NEAT_HCUBE_XOREXPERIMENTMAPELITES_H

#include "HCUBE_Experiment.h"

namespace HCUBE
{
    class XorExperimentMapElites : public Experiment
    {
    protected:

    public:
        XorExperimentMapElites(string _experimentName,int _threadID);

        virtual ~XorExperimentMapElites()
        {}

        virtual NEAT::GeneticPopulation* createInitialPopulation(int populationSize);

        virtual void processGroup(std::shared_ptr<NEAT::GeneticGeneration> generation);

        virtual void processIndividualPostHoc(std::shared_ptr<NEAT::GeneticIndividual> individual);

        virtual bool performUserEvaluations()
        {
            return false;
        }


        virtual inline bool isDisplayGenerationResult()
        {
            return displayGenerationResult;
        }

        virtual inline void setDisplayGenerationResult(bool _displayGenerationResult)
        {
            displayGenerationResult=_displayGenerationResult;
        }

        virtual inline void toggleDisplayGenerationResult()
        {
            displayGenerationResult=!displayGenerationResult;
        }

        virtual Experiment* clone();

        virtual void resetGenerationData(std::shared_ptr<NEAT::GeneticGeneration> generation)
        {}

        virtual void addGenerationData(std::shared_ptr<NEAT::GeneticGeneration> generation,std::shared_ptr<NEAT::GeneticIndividual> individual)
        {}

    };
}

#endif //