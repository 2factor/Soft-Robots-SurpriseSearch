#ifndef HCUBE_EVALUATIONSET_H_INCLUDED
#define HCUBE_EVALUATIONSET_H_INCLUDED

#include "HCUBE_Defines.h"

#include "Experiments/HCUBE_Experiment.h"

#include <mutex>

namespace HCUBE
{
    /**
    * EvaluationSet holds a group of individuals that are evaluated sequentially
    */
    class EvaluationSet
    {
    public:
    protected:
        bool running;
        std::shared_ptr<Experiment> experiment;
        std::shared_ptr<NEAT::GeneticGeneration> generation;
        vector<std::shared_ptr<NEAT::GeneticIndividual> >::iterator individualIterator;
        int individualCount;
        bool finished;
        std::mutex * mutex;

    public:

        EvaluationSet()
                :
                running(false),
                finished(false)
        {}

        EvaluationSet(
                std::shared_ptr<Experiment> _experiment,
                std::shared_ptr<NEAT::GeneticGeneration> _generation,
            vector<std::shared_ptr<NEAT::GeneticIndividual> >::iterator _individualIterator,
            int _individualCount
        )
                :
                running(false),
                experiment(_experiment),
                generation(_generation),
                individualIterator(_individualIterator),
                individualCount(_individualCount),
                finished(false),
                mutex(nullptr)
        {}

        EvaluationSet(
                std::shared_ptr<Experiment> _experiment,
                std::shared_ptr<NEAT::GeneticGeneration> _generation,
                vector<std::shared_ptr<NEAT::GeneticIndividual> >::iterator _individualIterator,
                int _individualCount, std::mutex * mutex
        )
                :
                running(false),
                experiment(_experiment),
                generation(_generation),
                individualIterator(_individualIterator),
                individualCount(_individualCount),
                finished(false),
                mutex(mutex)
        {}

        virtual ~EvaluationSet()
        {}

        /**
        * Group the individuals based on the experiment (some experiments support more than one individual per
        * evaluation) and then sequentially evaluate each group.
        */
        virtual void run();

        bool isFinished()
        {
            return finished;
        }

        /**
        * Returns if the EvaluationSet is running
        */
        bool getRunning()
        {
            return running;
        }

        void setRunning(bool _running)
        {
            running = _running;
        }
    protected:
    };
}

#endif // HCUBE_EVALUATIONSET_H_INCLUDED
