[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## Fusing Novelty and Surprise for Evolving Robot Morphologies

Source code for Fusing Novelty and Surprise for Evolving Robot Morphologies 

## Authors:

- Daniele Gravina (daniele.gravina@um.edu.mt)

## Publication:
- Gravina, Daniele, Antonios Liapis, and Georgios N. Yannakakis. "Fusing Novelty and Surprise for Evolving Robot Morphologies." in Proceedings of the Genetic and Evolutionary Computation Conference . ACM, 2018.

## Requirements

- C++11 compiler.

## IDE

- the source code has been written and tested with CLion 2017.2.3

## Additional Notes

The provided source code is based on the soft-robots-novelty-search source code provided in [1].

https://github.com/GiorgosMethe/Soft-Robots-Novelty-Search

## References

[1] Methenitis, G., Hennes, D., Izzo, D., & Visser, A. (2015, July). Novelty search for soft robotic space exploration. In Proceedings of the 2015 annual conference on Genetic and Evolutionary Computation (pp. 193-200). ACM.

----

