//
// Created by idgresearchp on 14/02/18.
//

#ifndef NEAT_NEAT_MAPELITES_H
#define NEAT_NEAT_MAPELITES_H

#include <vector>
#include <array>
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include <boost/multi_array.hpp>
#include <queue>

#include "NEAT_GeneticIndividual.h"
#include "NEAT_GeneticGeneration.h"
#include "NEAT.h"
#include "NEAT_SurpriseModel.h"

using namespace NEAT;

//TODO temporary
const int BEHAVIOURAL_DIM = 2;

class MapElites {

public:

    bool SOFT_ROBOTS = true;
    bool SURPRISE = false;
    bool SURPRISE_FITNESS = false;
    bool SURPRISE_FITNESS_MODEL = false;
    bool SURPRISE_MODEL = false;
    bool SURPRISE_BEHAVIOUR = false;
    bool NOVELTY = false;
    bool NOV_SUR = false;
    bool MULTI_NOV_SUR = true;

    AbstractSurpriseModel* surprise_model;
    //vector to hold novel individuals during the evolution
    vector<std::shared_ptr<GeneticIndividual> > novelIndividuals;

    std::unordered_map<int,  std::list< std::shared_ptr<GeneticIndividual> > > rank_fronts; //rank fronts (NSGA-II)

    int iterations = 0;

    typedef std::shared_ptr<GeneticIndividual> individual_ptr_t;

    typedef boost::multi_array<individual_ptr_t, BEHAVIOURAL_DIM> array_t;

    typedef boost::multi_array<int, BEHAVIOURAL_DIM> array_counter_t;

    typedef boost::multi_array<float, BEHAVIOURAL_DIM> array_surprise_t;

    typedef boost::multi_array<string, BEHAVIOURAL_DIM> array_ids_t;

    typedef std::shared_ptr<GeneticIndividual> indiv_t;
    typedef std::array<float, BEHAVIOURAL_DIM> point_t;

    typedef std::array<typename array_t::index, BEHAVIOURAL_DIM> behav_index_t;
    behav_index_t behav_shape;
    behav_index_t behav_shape_s;

    vector<std::shared_ptr<NEAT::GeneticIndividual>> population, p_parents;


//
    MapElites();

    virtual ~MapElites();

//    long int getindex(const array_t & m, const individual_ptr_t* requestedElement, const unsigned short int direction) const;
//
//    behav_index_t getindexarray(const array_t & m, const individual_ptr_t* requestedElement ) const;

    void update_archive();

    void epoch(int onGeneration);

    void printElites(int gen);

    void printPredictions();

    void generationData(int onGeneration);

    individual_ptr_t getInd(int i, int j)
    {
        behav_index_t behav_pos;
        behav_pos[0] = i;
        behav_pos[1] = j;

        return _array(behav_pos);
    }

    string getIndID(int i, int j)
    {
        behav_index_t behav_pos;
        behav_pos[0] = i;
        behav_pos[1] = j;

        return _array_ids(behav_pos);
    }

//    const array_t& archive() const;
//
//    const array_t& parents() const;
//
//    template<typename I> point_t get_point(const I& indiv) const;

protected:
    // archive of elite individuals
    array_t _array;
    array_t _array_parents;
    array_counter_t _array_counter;
    array_counter_t _array_selection;
    array_surprise_t _array_t_2, _array_t_1, _array_pred;
    array_ids_t _array_ids;

    bool _save_selection(indiv_t individual)
    {
        point_t p = _get_point(individual);

        behav_index_t behav_pos;
        for(size_t i = 0; i < BEHAVIOURAL_DIM; ++i) {
            behav_pos[i] = round(p[i] * behav_shape[i]);
            behav_pos[i] = std::min(behav_pos[i], behav_shape[i] - 1);
            assert(behav_pos[i] < behav_shape[i]);
        }

        if (!_array_selection(behav_pos))
            _array_selection(behav_pos) = 1;
        else
            _array_selection(behav_pos) += 1;

        return true;
    }

    bool _add_to_archive(indiv_t individual, indiv_t parent) {
        if(!individual->isValid())
            return false;

        point_t p = _get_point(individual);

        behav_index_t behav_pos;
        for(size_t i = 0; i < BEHAVIOURAL_DIM; ++i) {
            behav_pos[i] = round(p[i] * behav_shape[i]);
            behav_pos[i] = std::min(behav_pos[i], behav_shape[i] - 1);
            assert(behav_pos[i] < behav_shape[i]);
        }

        if (!_array_counter(behav_pos))
            _array_counter(behav_pos) = 1;
        else
            _array_counter(behav_pos) += 1;

        if (!_array(behav_pos)
            || (individual->getFitness() - _array(behav_pos)->getFitness()) > FLT_EPSILON
            || (fabs(individual->getFitness() - _array(behav_pos)->getFitness()) <= FLT_EPSILON
                && _dist_center(individual) < _dist_center(_array(behav_pos))) ) {
            _array(behav_pos) = individual;
            _array_parents(behav_pos) = parent;

            _array_ids(behav_pos) = individual->individualID;

            return true;
        }
        return false;
    }

    bool _update_surprise_model(indiv_t individual) {

        if(!individual->isValid())
            return false;

        point_t p = _get_point(individual);

        behav_index_t behav_pos;
        for(size_t i = 0; i < BEHAVIOURAL_DIM; ++i) {
            behav_pos[i] = round(p[i] * behav_shape_s[i]);
            behav_pos[i] = std::min(behav_pos[i], behav_shape_s[i] - 1);
            assert(behav_pos[i] < behav_shape_s[i]);
        }

        if (!_array_t_1(behav_pos))
            _array_t_1(behav_pos) = 1;
        else
            _array_t_1(behav_pos) += 1;

    }



    float _dist_center(const individual_ptr_t individual) {
        /* Returns distance to center of behavior descriptor cell */
        float dist = 0.0;
        point_t p = _get_point(individual);
        for(size_t i = 0; i < BEHAVIOURAL_DIM; ++i)
            dist += pow(p[i] - (float)round(p[i] * (float)(behav_shape[i] - 1))/(float)(behav_shape[i] - 1), 2);

        dist = sqrt(dist);
        return dist;
    }

    point_t _get_point(const individual_ptr_t individual) const {
        point_t p;
        for(size_t i = 0; i < BEHAVIOURAL_DIM; ++i) {
            p[i] = std::min(1.0f, individual->behaviorMat.signature->mat.at<float>((int) i));
        }

        return p;
    }

    //select randomly an individual from the elites
    indiv_t _selection(vector<std::shared_ptr<GeneticIndividual>> &ptmp) {

        if(NOV_SUR)
        {
            double lambda = double(NEAT::Globals::getSingleton()->getParameterValue("NoveltySurpriseLambda"));

            vector<std::shared_ptr<GeneticIndividual>> ptmp_2;

            float total_fitness = 0.0;

            //Sum all the fitness (novelty)
            //for the purposes of the roulette

            for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1")); ++i) {
                for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2")); ++j) {

                    behav_index_t behav_pos;
                    behav_pos[0] = i;
                    behav_pos[1] = j;

                    if (_array(behav_pos)) {

                        total_fitness += _array(behav_pos)->noveltyMapElites*lambda
                                         + _array(behav_pos)->surpriseMapElites*(1-lambda);

                        ptmp_2.emplace_back(_array(behav_pos));
                    }
                }
            }


            float marble = Globals::getSingleton()->getRandom().getRandomDouble()*total_fitness;
            //cout << marble << " " << marble*total_fitness << endl;
            sort(ptmp_2.begin(), ptmp_2.end(),
                 [lambda](const std::shared_ptr<NEAT::GeneticIndividual> lhs,
                    const std::shared_ptr<NEAT::GeneticIndividual> rhs)
                 { return lhs->noveltyMapElites*lambda + lhs->surpriseMapElites*(1-lambda)
                          < rhs->noveltyMapElites*lambda + rhs->surpriseMapElites*(1-lambda); }
            );

            auto curInd = ptmp_2.begin();
            float spin = (*curInd)->noveltyMapElites*lambda + (*curInd)->surpriseMapElites*(1-lambda);
            while(spin < marble) {
                ++curInd;

                //Keep the wheel spinning
                spin+=(*curInd)->noveltyMapElites*lambda + (*curInd)->surpriseMapElites*(1-lambda);
            }


            cout << "novelty value " << (*curInd)->noveltyMapElites
                 << " surprise value " << (*curInd)->surpriseMapElites << " lambda " << lambda << endl;

            return (*curInd);
        }

        if(MULTI_NOV_SUR)
        {
            double lambda = double(NEAT::Globals::getSingleton()->getParameterValue("NoveltySurpriseLambda"));

            vector<std::shared_ptr<GeneticIndividual>> ptmp_2;

            double total_fitness = 0.0;

            //Sum all the fitness (novelty)
            //for the purposes of the roulette

            for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1")); ++i) {
                for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2")); ++j) {

                    behav_index_t behav_pos;
                    behav_pos[0] = i;
                    behav_pos[1] = j;

                    if (_array(behav_pos)) {

                        total_fitness += 1/_array(behav_pos)->rank_fitness;

                        ptmp_2.emplace_back(_array(behav_pos));
                    }
                }
            }


            double marble = Globals::getSingleton()->getRandom().getRandomDouble()*total_fitness;
           // cout << marble << " " << marble*total_fitness << endl;
            sort(ptmp_2.begin(), ptmp_2.end(),
                 [](const std::shared_ptr<NEAT::GeneticIndividual> lhs,
                          const std::shared_ptr<NEAT::GeneticIndividual> rhs)
                 {
                    if (lhs->rank_fitness == rhs->rank_fitness)
                    {
                        return lhs->crowd_dist > rhs->crowd_dist;

                    } else{
                        return lhs->rank_fitness < rhs->rank_fitness;
                    }
                 }
            );

            auto curInd = ptmp_2.begin();
            double spin = 1/(*curInd)->rank_fitness;
            while(spin < marble) {
                ++curInd;

                if(curInd == ptmp_2.end())
                {
                    curInd--;
                    break;
                }

                //Keep the wheel spinning
                spin+=1/(*curInd)->rank_fitness;
            }


            cout << "novelty value " << (*curInd)->noveltyMapElites
                 << " surprise value " << (*curInd)->surpriseMapElites
                 << " 1/rank " << 1/(*curInd)->rank_fitness << endl;

            return (*curInd);
        }

        if(NOVELTY)
        {
            vector<std::shared_ptr<GeneticIndividual>> ptmp_2;

            float total_fitness = 0.0;

            //Sum all the fitness (novelty)
            //for the purposes of the roulette

            for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1")); ++i) {
                for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2")); ++j) {

                    behav_index_t behav_pos;
                    behav_pos[0] = i;
                    behav_pos[1] = j;

                    if (_array(behav_pos)) {

                        total_fitness += _array(behav_pos)->noveltyMapElites;

                        ptmp_2.emplace_back(_array(behav_pos));
                    }
                }
            }


            float marble = Globals::getSingleton()->getRandom().getRandomDouble()*total_fitness;
            //cout << marble << " " << marble*total_fitness << endl;
            sort(ptmp_2.begin(), ptmp_2.end(),
                 [](const std::shared_ptr<NEAT::GeneticIndividual> lhs,
                    const std::shared_ptr<NEAT::GeneticIndividual> rhs)
                 { return lhs->noveltyMapElites < rhs->noveltyMapElites; }
            );

            auto curInd = ptmp_2.begin();
            float spin = (*curInd)->noveltyMapElites;
            while(spin < marble) {
                ++curInd;

                //Keep the wheel spinning
                spin+=(*curInd)->noveltyMapElites;
            }


            cout << "novelty value " << (*curInd)->noveltyMapElites << endl;

            return (*curInd);
        }



        if(SURPRISE_BEHAVIOUR && iterations > 2)
        {
            vector<std::shared_ptr<GeneticIndividual>> ptmp_2;

            float total_fitness = 0.0;

            //Sum all the fitness (surprise)
            //for the purposes of the roulette

            for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1")); ++i) {
                for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2")); ++j) {

                    behav_index_t behav_pos;
                    behav_pos[0] = i;
                    behav_pos[1] = j;

                    if (_array(behav_pos)) {

                        total_fitness += _array(behav_pos)->surpriseMapElites;

                        ptmp_2.emplace_back(_array(behav_pos));
                    }
                }
            }


            float marble = Globals::getSingleton()->getRandom().getRandomDouble()*total_fitness;
            //cout << marble << " " << marble*total_fitness << endl;
            sort(ptmp_2.begin(), ptmp_2.end(),
                 [](const std::shared_ptr<NEAT::GeneticIndividual> lhs,
                    const std::shared_ptr<NEAT::GeneticIndividual> rhs)
                 { return lhs->surpriseMapElites < rhs->surpriseMapElites; }
            );

            auto curInd = ptmp_2.begin();
            float spin = (*curInd)->surpriseMapElites;
            while(spin < marble) {
                ++curInd;

                //Keep the wheel spinning
                spin+=(*curInd)->surpriseMapElites;
            }

            cout << "surprise value " << (*curInd)->surpriseMapElites << endl;

            return (*curInd);

        }

        if(SURPRISE_FITNESS_MODEL && iterations > 2)
        {

            vector<std::shared_ptr<GeneticIndividual>> ptmp_2;

            float total_fitness = 0.0;

            //Sum all the fitness (surprise)
            //for the purposes of the roulette

            for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1")); ++i) {
                for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2")); ++j) {

                    behav_index_t behav_pos;
                    behav_pos[0] = i;
                    behav_pos[1] = j;

                    if (_array(behav_pos)) {

                        surprise_model->evaluate_surprise(_array(behav_pos));

                        total_fitness += _array(behav_pos)->surpriseMapElites;

                        ptmp_2.emplace_back(_array(behav_pos));
                    }
                }
            }


            float marble = Globals::getSingleton()->getRandom().getRandomDouble()*total_fitness;
            //cout << marble << " " << marble*total_fitness << endl;
            sort(ptmp_2.begin(), ptmp_2.end(),
                 [](const std::shared_ptr<NEAT::GeneticIndividual> lhs,
                    const std::shared_ptr<NEAT::GeneticIndividual> rhs)
                 { return lhs->surpriseMapElites < rhs->surpriseMapElites; }
            );

            auto curInd = ptmp_2.begin();
            float spin = (*curInd)->surpriseMapElites;
            while(spin < marble) {
                ++curInd;

                //Keep the wheel spinning
                spin+=(*curInd)->surpriseMapElites;
            }


            cout << "surprise value " << (*curInd)->surpriseMapElites << endl;

            return (*curInd);
        }

        if(SURPRISE_MODEL && iterations > 2)
        {

            vector<std::shared_ptr<GeneticIndividual>> ptmp_2;

            float total_fitness = 0.0;

            //Sum all the fitness (surprise)
            //for the purposes of the roulette

            for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1")); ++i) {
                for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2")); ++j) {

                    behav_index_t behav_pos;
                    behav_pos[0] = i;
                    behav_pos[1] = j;

                    if (_array(behav_pos)) {

                        surprise_model->evaluate_surprise(_array(behav_pos));

                        total_fitness += _array(behav_pos)->surpriseMapElites;

                        ptmp_2.emplace_back(_array(behav_pos));
                    }
                }
            }


            float marble = Globals::getSingleton()->getRandom().getRandomDouble()*total_fitness;
            //cout << marble << " " << marble*total_fitness << endl;
            sort(ptmp_2.begin(), ptmp_2.end(),
                 [](const std::shared_ptr<NEAT::GeneticIndividual> lhs,
                    const std::shared_ptr<NEAT::GeneticIndividual> rhs)
                 { return lhs->surpriseMapElites < rhs->surpriseMapElites; }
            );

            auto curInd = ptmp_2.begin();
            float spin = (*curInd)->surpriseMapElites;
            while(spin < marble) {
                ++curInd;

                //Keep the wheel spinning
                spin+=(*curInd)->surpriseMapElites;
            }


            cout << "surprise value " << (*curInd)->surpriseMapElites << endl;

            return (*curInd);
        }

        if((SURPRISE || SURPRISE_FITNESS) && iterations > 2)
        {

            vector<std::shared_ptr<GeneticIndividual>> ptmp_2;

            float total_fitness = 0.0;

            //Sum all the fitness (surprise)
            //for the purposes of the roulette

            for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1")); ++i) {
                for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2")); ++j) {

                    behav_index_t behav_pos;
                    behav_pos[0] = i;
                    behav_pos[1] = j;

                    if (_array(behav_pos)) {

                        point_t p = _get_point(_array(behav_pos));

                        behav_index_t behav_pos_s;
                        for(size_t i = 0; i < BEHAVIOURAL_DIM; ++i) {
                            behav_pos_s[i] = round(p[i] * behav_shape_s[i]);
                            behav_pos_s[i] = std::min(behav_pos[i], behav_shape_s[i] - 1);
                            assert(behav_pos_s[i] < behav_shape_s[i]);
                        }

                        total_fitness += 1 - _array_pred(behav_pos_s);

                        _array(behav_pos)->surpriseMapElites = 1 - _array_pred(behav_pos_s);

                        ptmp_2.emplace_back(_array(behav_pos));
                    }
                }
            }


            float marble = Globals::getSingleton()->getRandom().getRandomDouble()*total_fitness;
            //cout << marble << " " << marble*total_fitness << endl;
            sort(ptmp_2.begin(), ptmp_2.end(),
                 [](const std::shared_ptr<NEAT::GeneticIndividual> lhs,
                    const std::shared_ptr<NEAT::GeneticIndividual> rhs)
                 { return lhs->surpriseMapElites < rhs->surpriseMapElites; }
            );

            auto curInd = ptmp_2.begin();
            float spin = (*curInd)->surpriseMapElites;
            while(spin < marble) {
                ++curInd;

                //Keep the wheel spinning
                spin+=(*curInd)->surpriseMapElites;
            }


            cout << "surprise value " << (*curInd)->surpriseMapElites << endl;

            return (*curInd);


        }

        cout << "random value " << endl;

        int random_index = Globals::getSingleton()->getRandom().getRandomWithinRange(0, ptmp.size() -1);
        return ptmp[random_index];

    }


    double getIndividualNovelty(std::shared_ptr<GeneticIndividual> individual,
                                vector<std::shared_ptr<GeneticIndividual>> &ptmp) {
        double NoveltyFitness = 0.0;
        // Keep only K smallest differences in behavior space
        std::priority_queue<double> sortedNovelIndividuals;
        int k = int(NEAT::Globals::getSingleton()->getParameterValue("NoveltyDistanceKNNvalue"));

        for (int i = 0; i < ptmp.size(); i++) {
            double tmp = individual->getSignatureDifference(ptmp.at(i));
            sortedNovelIndividuals.push(tmp);
            if (sortedNovelIndividuals.size() > k) sortedNovelIndividuals.pop();
        }

        for (int i = 0; i < novelIndividuals.size(); i++) {
            double tmp = individual->getSignatureDifference(novelIndividuals.at(i));
            sortedNovelIndividuals.push(tmp);
            if (sortedNovelIndividuals.size() > k) sortedNovelIndividuals.pop();
        }

        int size = sortedNovelIndividuals.size();

        while (sortedNovelIndividuals.size() > 0) {
            NoveltyFitness += (double) sortedNovelIndividuals.top();
            sortedNovelIndividuals.pop();
        }
        NoveltyFitness /= (double) size;

        return NoveltyFitness;
    }

    void sortNondominated(vector<std::shared_ptr<GeneticIndividual>> &ptmp) {
        std::list<std::shared_ptr<GeneticIndividual> > bestFront;

        std::list<std::shared_ptr<GeneticIndividual> > dominated;

        rank_fronts.clear();

        for (auto popIter = ptmp.begin(); popIter != ptmp.end(); popIter++) {

            dominated.clear();
            std::shared_ptr<GeneticIndividual> org = *popIter;
            org->dominationCount = 0;
            org->dominated.clear();

            for (auto innerIter = ptmp.begin(); innerIter != ptmp.end(); innerIter++) {

                if (*innerIter == *popIter)
                    continue;

                int dominationScore = org->dominates((*innerIter), (*popIter));

                if (dominationScore == -1) {
                    // *popIter dominates
                    dominated.emplace_back(*innerIter);
                }
                if (dominationScore == 1) {
                    // *innerIter dominates
                    org->dominationCount += 1;
                }
            }

            if (org->dominationCount == 0) {
                org->change_rank(1);
                bestFront.emplace_back(org);
            }

            org->dominated = dominated;

        }

        int currentRank = 1;

        std::list<std::shared_ptr<GeneticIndividual>> currentFront = bestFront;
        std::list<std::shared_ptr<GeneticIndividual>> nextFront;

        while (!currentFront.empty()) {
            nextFront.clear();

            for (auto frontIterator:currentFront) {

                dominated = frontIterator->dominated;

                for (auto it:dominated) {

                    it->dominationCount -= 1;
                    assert(it->dominationCount >= 0);

                    if (it->dominationCount == 0) {
                        it->change_rank(currentRank + 1);
                        nextFront.emplace_back(it);
                    }
                }
            }

            rank_fronts[currentRank - 1] = currentFront;

            currentRank += 1;

            currentFront = nextFront;
        }
    }

    void calculateCrowdingDistance(vector<std::shared_ptr<GeneticIndividual>> &ptmp) {

        int num_objectives = (int) NEAT::Globals::getSingleton()->getParameterValue("NUM_OBJECTIVES");

        double max = 0.0, min = 0.0;

        std::vector<std::shared_ptr<GeneticIndividual> > front;

        for (int i = 0; i < rank_fronts.size(); ++i) {

            /*cout << i + 1 << ' ' << rank_fronts[i].size() << endl;

            cout << "contains" << endl;

            for (auto org:rank_fronts[i]) {
                cout << org->fitnesses[0] << ", " << org->fitnesses[1]<< endl;
            }

            cout << endl;*/

            if (rank_fronts[i].empty()) {
                continue;
            }

            front.clear();

            for (auto org:rank_fronts[i]) {
                front.emplace_back(org);
                org->crowd_dist = 0;
            }

            for (int obj_index = 0; obj_index < num_objectives; ++obj_index) {
                std::sort(front.begin(), front.end(),
                          [obj_index](std::shared_ptr<GeneticIndividual> const &a,
                                      std::shared_ptr<GeneticIndividual> const &b)
                                  -> bool {
                              return a->fitnesses[obj_index] < b->fitnesses[obj_index];
                          }
                );


                min = front[0]->fitnesses[obj_index];
                max = front[front.size() - 1]->fitnesses[obj_index];

                //initialization
                front[0]->crowd_dist = 999999999;
                front[front.size() - 1]->crowd_dist = 999999999;

                for (int index = 1; index < front.size() - 1; ++index) {
                    front[index]->crowd_dist +=
                            (front[index + 1]->fitnesses[obj_index] - front[index - 1]->fitnesses[obj_index]) /
                            (max - min + 0.1);
                }
            }

        }
    }



};

#endif //NEAT_NEAT_MAPELITES_H
