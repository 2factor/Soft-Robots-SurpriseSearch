#ifndef SURPRISE_MATH_H
#define	SURPRISE_MATH_H

#include <vector>
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"

#include "NEAT_GeneticIndividual.h"
#include "NEAT_GeneticGeneration.h"

#include <boost/multi_array.hpp>

using namespace NEAT;

class AbstractSurpriseModel {
protected:
    // no variables it seems
public:

    virtual ~AbstractSurpriseModel() = 0;

    virtual void updateModel(vector<std::shared_ptr<GeneticGeneration> > &generations, int onGeneration) = 0;

    virtual void updateModel(boost::multi_array<float, 2> map){};

    virtual double evaluate_surprise(std::shared_ptr<GeneticIndividual> &ind) = 0;

};

inline AbstractSurpriseModel::~AbstractSurpriseModel() {}

class KMeansModel : public AbstractSurpriseModel {
protected:
    //ofstream model_savefile, centroid_savefile,pred_centroid_savefile, fitness_savefile;
    vector<cv::Mat> prev_centroids;
    vector<cv::Mat> predicted_centroids;
    TiXmlDocument doc;
    TiXmlElement *elem;
    int num_cluster;
    int size_behaviour = -1;

public:
    KMeansModel(int num_cluster);

    ~KMeansModel();

    void initLogs(char *output_dir);

    void updateModel(vector<std::shared_ptr<GeneticGeneration> > &generations, int onGeneration);

    double evaluate_surprise(std::shared_ptr<GeneticIndividual> &ind);

    //void saveFitness(float novelty, float surprise);

    //void saveFitness(float novelty, float surprise, float objective);

};

cv::Mat predictNextPoint(cv::Mat &prev, cv::Mat &curr,  cv::Mat &prediction);

cv::Mat calculateCentroid(cv::Mat &trajectories);

#endif	/* SURPRISE_MATH_H */