//
// Created by idgresearch on 12/06/18.
//

#ifndef NEAT_NEAT_DIRECTREPRESENTATION_H
#define NEAT_NEAT_DIRECTREPRESENTATION_H

#include "NEAT_Defines.h"
#include "NEAT_STL.h"
#include "tinyxmlplus.h"
#include <typeinfo>

#include "NEAT_Network.h"
#include "NEAT_ModularNetwork.h"
#include "NEAT_FastNetwork.h"
#include "NEAT_GeneticIndividualBehavior.h"
#ifdef EPLEX_INTERNAL
#include "NEAT_VectorNetwork.h"
#include "NEAT_FractalNetwork.h"
#endif

namespace NEAT {

    class DirectRepresentation {

    protected:

        double fitness; // Can be fitness or novelty

    public:

        GeneticIndividualBehavior behavior;

        string individualID;

        vector<int> genome;

        NEAT_DLL_EXPORT DirectRepresentation(
        );

        /**
        * Copy an individual. THIS COPIES FITNESS TOO!  DO NOT USE THIS TO MAKE OFFSPRING!
       */
        NEAT_DLL_EXPORT DirectRepresentation(DirectRepresentation &copy);

        NEAT_DLL_EXPORT virtual ~DirectRepresentation();

        NEAT_DLL_EXPORT void cross(std::shared_ptr<DirectRepresentation> parent2,
                                         std::shared_ptr<DirectRepresentation> child1,
                                         std::shared_ptr<DirectRepresentation> child2);

        NEAT_DLL_EXPORT void mutate();

        inline void setFitness(double _fitness)
        {
            fitness = _fitness;
        }

        inline double getFitness()
        {
            return fitness;
        }

    };
}

#endif //NEAT_NEAT_DIRECTREPRESENTATION_H
