#ifndef __GENETICPOPULATION_H__
#define __GENETICPOPULATION_H__

#include "tinyxmlplus.h"
#include "NEAT_STL.h"

#include "NEAT_Globals.h"
#include "NEAT_GeneticSpecies.h"

#include "NEAT_SurpriseModel.h"

#ifdef EPLEX_INTERNAL
#include "NEAT_CoEvoExperiment.h"
#endif

namespace NEAT
{
    //fixme -> move me in the right file
    enum Experiment {NOV_SUR, NOV_OBJ, SUR_OBJ, NOV_LOC, SUR_LOC, NOV_SUR_OBJ, NOV_SUR_LOC, SUR_PRED_LOC};

    extern Experiment experiment;

    /**
     * The Genetic Population class is responsible for holding and managing a population of individuals
     * over multiple generations.
     */
    class GeneticPopulation
    {
        vector<std::shared_ptr<GeneticGeneration> > generations;

        vector<std::shared_ptr<GeneticSpecies> > species;

        //we use a separate vector for extinct species to save CPU.
        vector<std::shared_ptr<GeneticSpecies> > extinctSpecies;

        //vector to hold novel individuals during the evolution
        vector<std::shared_ptr<GeneticIndividual> > novelIndividuals;

        int onGeneration;

        double BestEverFitnessBackUp;

        AbstractSurpriseModel* surprise_model;

    public:
        NEAT_DLL_EXPORT GeneticPopulation();

        NEAT_DLL_EXPORT GeneticPopulation(string fileName);

#ifdef EPLEX_INTERNAL
        NEAT_DLL_EXPORT GeneticPopulation(std::shared_ptr<CoEvoExperiment> experiment);

        NEAT_DLL_EXPORT GeneticPopulation(string fileName,std::shared_ptr<CoEvoExperiment> experiment);
#endif

        NEAT_DLL_EXPORT virtual ~GeneticPopulation();

        inline std::shared_ptr<GeneticGeneration> getGeneration(int generationIndex=-1)
        {
            if (generationIndex==-1)
                generationIndex=int(onGeneration);

            return generations[generationIndex];
        }


        NEAT_DLL_EXPORT void noveltySearchComputation();

        //added by Gravina
        NEAT_DLL_EXPORT void surpriseSearchComputation();

        NEAT_DLL_EXPORT void multiObjectiveComputation();

        NEAT_DLL_EXPORT double computeNovelty(std::shared_ptr<GeneticIndividual> &individual);

        NEAT_DLL_EXPORT double computeSurprise(std::shared_ptr<GeneticIndividual> &individual);

        NEAT_DLL_EXPORT double computeNoveltySurprise(std::shared_ptr<GeneticIndividual> &individual);

        NEAT_DLL_EXPORT void noveltySurpriseSearchComputation();

        NEAT_DLL_EXPORT void fitnessSearchNoveltyComputation();

        NEAT_DLL_EXPORT void computeBehaviorSparsity();

        NEAT_DLL_EXPORT void addIndividual(std::shared_ptr<GeneticIndividual> individual);

        NEAT_DLL_EXPORT void generationData();

        NEAT_DLL_EXPORT int getIndividualCount(int generation=-1);

        NEAT_DLL_EXPORT std::shared_ptr<GeneticIndividual> getIndividual(int individualIndex,int generation=-1);

        NEAT_DLL_EXPORT vector<std::shared_ptr<GeneticIndividual> >::iterator getIndividualIterator(int a,int generation=-1);

        NEAT_DLL_EXPORT vector<std::shared_ptr<GeneticIndividual> > getNovelIndividuals()
        {
            return novelIndividuals;
        }

        NEAT_DLL_EXPORT std::shared_ptr<GeneticIndividual> getBestAllTimeIndividual();

        NEAT_DLL_EXPORT std::shared_ptr<GeneticIndividual> getBestAllTimeIndividualFitnessBackUp();

        NEAT_DLL_EXPORT std::shared_ptr<GeneticIndividual> getBestIndividualOfGeneration(int generation=LAST_GENERATION);

        inline std::shared_ptr<GeneticSpecies> getSpecies(int id)
        {
            for (int a=0;a<(int)species.size();a++)
            {
                if (species[a]->getID()==id)
                    return species[a];
            }

            for (int a=0;a<(int)species.size();a++)
            {
                cout << species[a]->getID() << endl;
            }

            cout << id << endl;

            throw CREATE_LOCATEDEXCEPTION_INFO("Tried to get a species which doesn't exist (Maybe it went extinct?)");
        }

        NEAT_DLL_EXPORT void recomputeSpecies();

        NEAT_DLL_EXPORT void speciate();

        NEAT_DLL_EXPORT void setSpeciesMultipliers();

        NEAT_DLL_EXPORT void adjustFitness();

        NEAT_DLL_EXPORT void adjustFitnessMulti();

        NEAT_DLL_EXPORT void produceNextGeneration();

        NEAT_DLL_EXPORT void produceNextGenerationMulti();

        NEAT_DLL_EXPORT void selectNonDominated();

        NEAT_DLL_EXPORT void dump(string filename,bool includeGenes,bool doGZ);

        NEAT_DLL_EXPORT void dumpBest(string filename,bool includeGenes,bool doGZ);

        NEAT_DLL_EXPORT void cleanupOld(int generationSkip);

        inline int getGenerationCount()
        {
            return (int)generations.size();
        }
    private:
        double getIndividualNovelty(std::shared_ptr<GeneticIndividual> individual, std::shared_ptr<GeneticGeneration> generation);
        double getIndividualLocalCompetition(std::shared_ptr<GeneticIndividual> individual, std::shared_ptr<GeneticGeneration> generation);
    };

}

#endif
