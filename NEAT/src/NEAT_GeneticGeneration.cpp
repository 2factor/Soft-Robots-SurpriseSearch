#include "NEAT_Defines.h"

#include "NEAT_GeneticGeneration.h"

#include "NEAT_GeneticLinkGene.h"
#include "NEAT_Globals.h"

namespace NEAT {
    GeneticGeneration::GeneticGeneration(int _generationNumber)
            :
            generationNumber(_generationNumber),
            sortedByFitness(false),
            isCached(false) {}

    GeneticGeneration::GeneticGeneration(
            GeneticGeneration *previousGeneration,
            const vector<std::shared_ptr<GeneticIndividual> > &newIndividuals,
            int _generationNumber)
            :
            individuals(newIndividuals),
            generationNumber(_generationNumber),
            sortedByFitness(false),
            isCached(false) {}

    GeneticGeneration::GeneticGeneration(TiXmlElement *generationElement)
            :
            isCached(true),
            sortedByFitness(true) {
        generationNumber = atoi(generationElement->Attribute("GenNumber"));

        generationElement->Attribute("AverageFitness", &cachedAverageFitness);

        TiXmlElement *individualElement = generationElement->FirstChildElement("Individual");

        do {
            individuals.push_back(std::shared_ptr<GeneticIndividual>(new GeneticIndividual(individualElement)));

            individualElement = individualElement->NextSiblingElement("Individual");
        } while (individualElement != NULL);

    }

    GeneticGeneration::GeneticGeneration(const GeneticGeneration &other) {
        //cout_ << "Invoking copy constructor...";
        generationNumber = other.generationNumber;
        sortedByFitness = other.sortedByFitness;

        isCached = other.isCached;
        cachedAverageFitness = other.cachedAverageFitness;

        userData = other.userData;

        individuals = other.individuals;

        //cout_ << "done!\n";
    }

    std::shared_ptr<GeneticGeneration> GeneticGeneration::produceNextGeneration(
            const vector<std::shared_ptr<GeneticIndividual> > &newIndividuals,
            int _generationNumber
    ) {
        return std::shared_ptr<GeneticGeneration>(
                new GeneticGeneration(
                        this,
                        newIndividuals,
                        _generationNumber
                )
        );
    }

    GeneticGeneration &GeneticGeneration::operator=(const GeneticGeneration &other) {
        //cout_ << "Invoking assignment operator...";
        generationNumber = other.generationNumber;
        sortedByFitness = other.sortedByFitness;

        isCached = other.isCached;
        cachedAverageFitness = other.cachedAverageFitness;

        userData = other.userData;

        individuals = other.individuals;

        //cout_ << "done!\n";
        return *this;
    }

    GeneticGeneration::~GeneticGeneration() {
    }

    void GeneticGeneration::setAttributes(TiXmlElement *generationElement) {
        generationElement->SetAttribute("GenNumber", int(generationNumber));

        generationElement->SetAttribute("UserData", userData);

        double totalFitness = 0;

        vector<int> speciesIDs;

        for (int a = 0; a < (int) individuals.size(); a++) {
            totalFitness += individuals[a]->getFitness();

            for (int b = 0; b <= (int) speciesIDs.size(); b++) {
                if (b == (int) speciesIDs.size()) {
                    speciesIDs.push_back(individuals[a]->getSpeciesID());
                    break;
                } else if (speciesIDs[b] == individuals[a]->getSpeciesID()) {
                    break;
                }
            }
        }

        if (isCached) {
            generationElement->SetDoubleAttribute("AverageFitness", cachedAverageFitness);
        } else {
            generationElement->SetDoubleAttribute("AverageFitness", totalFitness / individuals.size());
        }

        generationElement->SetAttribute("SpeciesCount", int(speciesIDs.size()));

    }

    void GeneticGeneration::dump(TiXmlElement *generationElement, bool includeGenes) {
        setAttributes(generationElement);

        for (int a = 0; a < (int) individuals.size(); a++) {
            TiXmlElement *individualElement = new TiXmlElement("Individual");

            individuals[a]->dump(individualElement, includeGenes);

            generationElement->LinkEndChild(individualElement);
        }
    }

    void GeneticGeneration::dumpBest(TiXmlElement *generationElement, bool includeGenes) {
        setAttributes(generationElement);

        std::shared_ptr<GeneticIndividual> bestIndividual = individuals[0];

        for (int a = 1; a < (int) individuals.size(); a++) {
            if (individuals[a]->getFitness() > bestIndividual->getFitness())
                bestIndividual = individuals[a];
        }

        TiXmlElement *individualElement = new TiXmlElement("Individual");

        bestIndividual->dump(individualElement, includeGenes);

        generationElement->LinkEndChild(individualElement);
    }

    std::shared_ptr<GeneticIndividual> GeneticGeneration::mateIndividuals(int i1, int i2) {
        std::shared_ptr<GeneticIndividual> ind1 = individuals[i1];
        std::shared_ptr<GeneticIndividual> ind2 = individuals[i2];

        return std::shared_ptr<GeneticIndividual>(new GeneticIndividual(ind1, ind2));
    }

    double GeneticGeneration::getCompatibility(int i1, int i2) {
        std::shared_ptr<GeneticIndividual> ind1 = individuals[i1];
        std::shared_ptr<GeneticIndividual> ind2 = individuals[i2];
        return ind1->getCompatibility(ind2);
    }

    void GeneticGeneration::sortByFitness() {
        for (int a = 0; a < (int) individuals.size(); a++) {
            for (int b = 0; b < ((int) individuals.size() - (a + 1)); b++) {
                if (individuals[b]->getFitness() < individuals[b + 1]->getFitness()) {
                    std::shared_ptr<GeneticIndividual> ind = individuals[b];
                    individuals[b] = individuals[b + 1];
                    individuals[b + 1] = ind;
                }
            }
        }

        sortedByFitness = true;
    }

    void GeneticGeneration::sortNondominated() {
        std::list<std::shared_ptr<GeneticIndividual> > bestFront;

        std::list<std::shared_ptr<GeneticIndividual> > dominated;

        rank_fronts.clear();

        for (auto popIter = individuals.begin(); popIter != individuals.end(); popIter++) {

            dominated.clear();
            std::shared_ptr<GeneticIndividual> org = *popIter;
            org->dominationCount = 0;
            org->dominated.clear();

            for (auto innerIter = individuals.begin(); innerIter != individuals.end(); innerIter++) {

                if (*innerIter == *popIter)
                    continue;

                int dominationScore = org->dominates((*innerIter), (*popIter));

                if (dominationScore == -1) {
                    // *popIter dominates
                    dominated.emplace_back(*innerIter);
                }
                if (dominationScore == 1) {
                    // *innerIter dominates
                    org->dominationCount += 1;
                }
            }

            if (org->dominationCount == 0) {
                org->change_rank(1);
                bestFront.emplace_back(org);
            }

            org->dominated = dominated;

        }

        int currentRank = 1;

        std::list<std::shared_ptr<GeneticIndividual>> currentFront = bestFront;
        std::list<std::shared_ptr<GeneticIndividual>> nextFront;

        while (!currentFront.empty()) {
            nextFront.clear();

            for (auto frontIterator:currentFront) {

                dominated = frontIterator->dominated;

                for (auto it:dominated) {

                    it->dominationCount -= 1;
                    assert(it->dominationCount >= 0);

                    if (it->dominationCount == 0) {
                        it->change_rank(currentRank + 1);
                        nextFront.emplace_back(it);
                    }
                }
            }

            rank_fronts[currentRank - 1] = currentFront;

            currentRank += 1;

            currentFront = nextFront;
        }
    }

    void GeneticGeneration::calculateCrowdingDistance() {

        int num_objectives = (int) NEAT::Globals::getSingleton()->getParameterValue("NUM_OBJECTIVES");

        double max = 0.0, min = 0.0;

        std::vector<std::shared_ptr<GeneticIndividual> > front;

        for (int i = 0; i < rank_fronts.size(); ++i) {

            /*cout << i + 1 << ' ' << rank_fronts[i].size() << endl;

            cout << "contains" << endl;

            for (auto org:rank_fronts[i]) {
                cout << org->fitnesses[0] << ", " << org->fitnesses[1]<< endl;
            }

            cout << endl;*/

            if (rank_fronts[i].empty()) {
                continue;
            }

            front.clear();

            for (auto org:rank_fronts[i]) {
                front.emplace_back(org);
                org->crowd_dist = 0;
            }

            for (int obj_index = 0; obj_index < num_objectives; ++obj_index) {
                std::sort(front.begin(), front.end(),
                          [obj_index](std::shared_ptr<GeneticIndividual> const &a,
                                      std::shared_ptr<GeneticIndividual> const &b)
                                  -> bool {
                              return a->fitnesses[obj_index] < b->fitnesses[obj_index];
                          }
                );


                min = front[0]->fitnesses[obj_index];
                max = front[front.size() - 1]->fitnesses[obj_index];

                //initialization
                front[0]->crowd_dist = 999999999;
                front[front.size() - 1]->crowd_dist = 999999999;

                for (int index = 1; index < front.size() - 1; ++index) {
                    front[index]->crowd_dist +=
                            (front[index + 1]->fitnesses[obj_index] - front[index - 1]->fitnesses[obj_index]) /
                            (max - min + 0.1);
                }
            }

#ifdef _DEBUG
            for(int index = 0; index < front.size(); ++index)
            {
                for(int obj_index = 0; obj_index < num_objectives; ++obj_index)
                {
                    cout << front[index]->fitnesses[obj_index] << " ";
                }

                cout << "#" ;
            }

            cout << endl;

            for(int index = 0; index < front.size(); ++index)
            {
                cout << front[index]->crowd_dist << " ";
            }

            cout << endl;
#endif
        }
    }

//Gets the generation champion.  Based on unadjusted Fitness
    std::shared_ptr<GeneticIndividual> GeneticGeneration::getGenerationChampion() {
        std::shared_ptr<GeneticIndividual> bestIndividual;
        for (int b = 0; b < (int) individuals.size(); b++) {
            if (!bestIndividual || individuals[b]->getFitness() > bestIndividual->getFitness()) {
                bestIndividual = individuals[b];
            }
        }
        return bestIndividual;
    }

//Gets the generation champion.  Based on unadjusted Fitness
    std::shared_ptr<GeneticIndividual> GeneticGeneration::getGenerationChampionFitnessBackUp() {
        std::shared_ptr<GeneticIndividual> bestIndividual;
        for (int b = 0; b < (int) individuals.size(); b++) {
            if (!bestIndividual || individuals[b]->getFitnessBackUp() > bestIndividual->getFitnessBackUp()) {
                bestIndividual = individuals[b];
            }
        }
        return bestIndividual;
    }

    void GeneticGeneration::cleanup() {

        if (!sortedByFitness) {
            throw CREATE_LOCATEDEXCEPTION_INFO("You aren't supposed to acll this until you sort by fitness!");
        }
        while (individuals.size() > 1) {
            //cout_ << "CLEANING UP!\n";
            //Never delete the generation champion (remember that they are sorted by fitness at this point).
            individuals.erase(individuals.begin() + 1);
        }
    }

    void GeneticGeneration::randomizeIndividualOrder() {
        double *randPos = new double[individuals.size()];

        for (int a = 0; a < int(individuals.size()); a++) {
            randPos[a] = Globals::getSingleton()->getRandom().getRandomDouble();
        }

        for (int a = 0; a < int(individuals.size()); a++) {
            for (int b = 0; b < int(individuals.size()) - 1; b++) {
                if (randPos[b] > randPos[b + 1]) {
                    swap(randPos[b], randPos[b + 1]);
                    swap(individuals[b], individuals[b + 1]);
                }
            }
        }

        sortedByFitness = false;

        delete[] randPos;
    }
}
