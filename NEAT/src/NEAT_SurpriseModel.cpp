//
// Created by Daniele on 17/10/2016.
//

#include "NEAT_SurpriseModel.h"

#include <gsl/gsl_fit.h>
#include <gsl/gsl_multifit.h>


KMeansModel::KMeansModel(int num_cluster)
{

    TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "", "" );

    doc.LinkEndChild( decl );

    elem = new TiXmlElement( "Centroids" );

    doc.LinkEndChild( elem );

    this->num_cluster = num_cluster;


}

KMeansModel::~KMeansModel() {

    //doc.SaveFile( "Centroids.xml" );

}

void KMeansModel::updateModel(vector<std::shared_ptr<GeneticGeneration>> &generations, int onGeneration) {


    cout << "calling update model..." << endl;

    //do the prediction by taking previous 2 generations

    cout << "prediction" << endl;

    if (prev_centroids.size() >= 2) {

        predicted_centroids.clear();

        TiXmlElement * pred = new TiXmlElement( "Predictions " );

        ostringstream  heatmap_string;

        int num_possible_cluster = prev_centroids[prev_centroids.size() - 2].rows < num_cluster ?
                                   prev_centroids[prev_centroids.size() - 2].rows :
                                   num_cluster;

        num_possible_cluster = prev_centroids[prev_centroids.size() - 1].rows < num_possible_cluster ?
                               prev_centroids[prev_centroids.size() - 1].rows :
                               num_possible_cluster;

        cout << "num_possible_cluster "  << num_possible_cluster << endl;

        for(int i = 0; i < num_possible_cluster; ++i)
        {

            cv::Mat prediction = cv::Mat::zeros(this->size_behaviour, 1, CV_64FC2);

            for (int k = 0; k < prediction.rows; ++k) {

                prediction.at<cv::Vec2d>((int) k) =
                        prev_centroids[prev_centroids.size() - 1].at<cv::Vec2d>(i, k) -
                        prev_centroids[prev_centroids.size() - 2].at<cv::Vec2d>(i, k) +
                        prev_centroids[prev_centroids.size() - 1].at<cv::Vec2d>(i, k);

            }

            predicted_centroids.emplace_back(prediction) ;

            for (int j = 0; j < predicted_centroids.back().rows; ++j){
                heatmap_string << predicted_centroids.back().at<cv::Vec2d>(j) << ", ";
            }

            heatmap_string << "# ";
        }

        TiXmlText * text = new TiXmlText( heatmap_string.str() );

        pred->LinkEndChild( text );

        elem->LinkEndChild(pred);


    }

    //just get the first valid individual
    if(this->size_behaviour == -1) {
        for (int a = 0; a < generations[onGeneration]->getIndividualCount(); a++) {

            if (generations[onGeneration]->getIndividual(a)->isValid() &&
                generations[onGeneration]->getIndividual(a)->behavior.validBehavior) {

                if (this->size_behaviour == -1) {
                    this->size_behaviour = generations[onGeneration]->getIndividual(a)->behavior.signature->mat.rows;
                    break;
                }
            }
        }
    }


    int valid_individuals = 0;

    cv::Mat X;

    for (int a = 0; a < generations[onGeneration]->getIndividualCount(); a++) {

        if (generations[onGeneration]->getIndividual(a)->isValid() &&
            generations[onGeneration]->getIndividual(a)->behavior.validBehavior) {

            valid_individuals++;
        }
    }

    X.create(valid_individuals, this->size_behaviour, CV_64FC2);

    int index = 0;

    TiXmlElement * inds = new TiXmlElement( "Individuals" );

    ostringstream  individuals;


    //get all individuals in this generation and create X
    for (int a = 0; a < generations[onGeneration]->getIndividualCount(); a++) {

        if (generations[onGeneration]->getIndividual(a)->isValid() &&
            generations[onGeneration]->getIndividual(a)->behavior.validBehavior) {

            if (generations[onGeneration]->getIndividual(a)->behavior.signature->mat.type() == CV_64FC2) {
                for (int i = 0; i < this->size_behaviour; i++) {
                    X.at<cv::Vec2d>(index, i) = generations[onGeneration]->getIndividual(a)->behavior.signature->mat.at<cv::Vec2d>((int) i);

                    individuals <<  X.at<cv::Vec2d>(index, i) << ", ";
                }

                individuals << "# ";
            }

            index++;

        }
    }

    TiXmlText * text = new TiXmlText( individuals.str() );

    inds->LinkEndChild( text );

    elem->LinkEndChild(inds);

    cout << "valid individuals " << valid_individuals << endl;

    if(valid_individuals < num_cluster) {
        cout << "jumping this generation, not enough valid individuals" << endl;

        TiXmlElement * generation = new TiXmlElement( "Generation " );

        TiXmlText *text = new TiXmlText( "Jumped");

        generation->LinkEndChild( text );

        elem->LinkEndChild(generation);

        //doc.SaveFile( "Centroids.xml" );
        return;
    }


    cv::Mat currentcentroid = calculateCentroid(X);

    /*subtract mean from current positions*/
    for(int j = 0; j < currentcentroid.cols; ++j)
    {
        for(int i = 0; i < X.rows; ++i) {
            X.at<cv::Vec2d>(i, j) -= currentcentroid.at<cv::Vec2d>(0, j);
        }
    }

    cv::Mat centroids ;
    centroids.create(num_cluster, this->size_behaviour, CV_64FC2);

    cout_  << "X"  << endl;

    cout_ << X  << endl;

    int num_possible_cluster = num_cluster < valid_individuals ?
                               num_cluster :
                               valid_individuals;

    if(prev_centroids.size() >= 1)
    {

        /*seed from previous generation kmeans*/
        for(int i = 0; i < prev_centroids[prev_centroids.size() - 1].rows; ++i)
        {
            for(int j = 0; j < X.cols; ++j)
            {
                centroids.at<cv::Vec2d>(i, j) = prev_centroids[prev_centroids.size() - 1].at<cv::Vec2d>(i, j);

                /*subtract mean*/
                centroids.at<cv::Vec2d>(i, j) -= currentcentroid.at<cv::Vec2d>(0, j);
            }

            //k_means_log << "centroid " << i << " " << centroids_x[i] << " " << centroids_y[i] << endl;
        }

        if(prev_centroids[prev_centroids.size() - 1].rows < num_possible_cluster)
        {
            vector<int> indexes(X.rows);

            for(int i = 0; i < X.rows; ++i)
            {
                indexes[i] = i;
            }

            random_shuffle ( indexes.begin(), indexes.end() );

            for(int i = prev_centroids[prev_centroids.size() - 1].rows; i < num_possible_cluster - prev_centroids[prev_centroids.size() - 1].rows; ++i)
            {
                for(int j = 0; j < X.cols; ++j)
                {
                    centroids.at<cv::Vec2d>(i, j) = X.at<cv::Vec2d>(indexes[i], j);

                    //k_means_log << "centroid " << i << " " << centroids_x[i] + currentcentroid[0] << " " << centroids_y[i] + currentcentroid[1] << endl;
                }
            }
        }

        cout_  << "centroids"  << endl;

        cout_ << centroids << endl;

        //k_means_log << endl;
    }
    else
    {
        /*gen = 0 -> random initialization of centroids*/

        vector<int> indexes(valid_individuals);

        for(int i = 0; i < valid_individuals; ++i)
        {
            indexes[i] = i;
        }

        random_shuffle ( indexes.begin(), indexes.end() );

        for(int i = 0; i < num_possible_cluster; ++i)
        {
            for(int j = 0; j < X.cols; ++j)
            {
                centroids.at<cv::Vec2d>(i, j) = X.at<cv::Vec2d>(indexes[i], j);

                //k_means_log << "centroid " << i << " " << centroids_x[i] + currentcentroid[0] << " " << centroids_y[i] + currentcentroid[1] << endl;
            }
        }

        cout_  << "centroids"  << endl;

        cout_ << centroids << endl;

        //k_means_log << endl;
    }

    int iter_counter = 0;
    double squared_norm = 0.0;
    double best_error = numeric_limits<double>::max();
    double old_error, error = 0.0;

    cv::Mat temp_centroids, old_centroids, best_centroids;

    temp_centroids = cv::Mat::zeros(num_possible_cluster, this->size_behaviour, CV_64FC2);
    old_centroids = cv::Mat::zeros(num_possible_cluster, this->size_behaviour, CV_64FC2);
    best_centroids = cv::Mat::zeros(num_possible_cluster, this->size_behaviour, CV_64FC2);

    //k_means_log << "begin loop" << endl;

    vector<bool> points_taken(X.rows, false);
    vector<int> centroids_not_taken;
    vector<int> counts_cluster(num_possible_cluster, 0);
    vector<int> labels(X.rows, -1);
    vector<int> best_labels(X.rows, -1);


    do {
        /* save error from last step */
        old_error = error;
        error = 0.0;
        squared_norm = 0.0;

        //k_means_log << "iter " << iter_counter << endl;

        /* clear old counts and temp centroids */
        for (int i = 0; i < num_possible_cluster; i++) {
            counts_cluster[i] = 0;
        }

        temp_centroids = cv::Mat::zeros(num_possible_cluster, this->size_behaviour, CV_64FC2);

        /*safe check- > all points assigned*/
        std::fill(labels.begin(), labels.end(), -1);

        for (int h = 0; h < X.rows; ++h) {
            /* identify the closest cluster to the point h */
            double min_distance = numeric_limits<double>::max();
            double dist = 0.0;

            cout_ << "point: " << h << endl;

            for (int i = 0; i < num_possible_cluster; ++i) {

                dist = 0.0;

                for(int j = 0; j < X.cols; ++j)
                {
                    cv::Vec2d diffV2d = X.at<cv::Vec2d>((int) h, j) - centroids.at<cv::Vec2d>((int) i, j);

                    dist += pow(diffV2d[0], 2) + pow(diffV2d[1], 2);
                }

                if (dist < min_distance) {
                    labels[h] = i;
                    min_distance = dist;
                }

                cout_ << "cluster: " << i <<" dist: " << dist << endl;
            }

            cout_ << "point " << h << " cluster " << labels[h] << " dist " << min_distance << endl;

            /* update size and temp centroid of the destination cluster */


            for(int j = 0; j < X.cols; ++j)
            {
                temp_centroids.at<cv::Vec2d>(labels[h], j) += X.at<cv::Vec2d>(h, j);
            }


            counts_cluster[labels[h]]++;
            /* update standard error */
            error += min_distance;
        }

        cout_ << "labels" << endl;

        for(int l = 0; l < labels.size(); ++l)
        {
            cout_ << labels[l] << " ";
        }
        cout_ << endl;

        cout_ << "counts_cluster" << endl;

        for(int l = 0; l < counts_cluster.size(); ++l) {
            cout_ << " counts : " << counts_cluster[l] << " ";
        }

        cout_ << endl;



        for (int i = 0; i < num_possible_cluster; i++) { /* update all centroids */

            for (int j = 0; j < X.cols; ++j) {
                old_centroids.at<cv::Vec2d>(i, j) = centroids.at<cv::Vec2d>(i, j);
            }


            if (counts_cluster[i]) {

                for (int j = 0; j < X.cols; ++j) {
                    centroids.at<cv::Vec2d>(i, j)[0] = double (temp_centroids.at<cv::Vec2d>(i, j)[0] / counts_cluster[i]);
                    centroids.at<cv::Vec2d>(i, j)[1] = double (temp_centroids.at<cv::Vec2d>(i, j)[1] / counts_cluster[i]);
                }


                cout << "centroid " << i << " " << "taken" << endl;

            } else {

                cout << "centroid " << i << " " << "not taken" << endl;
            }
        }


        for(int i = 0; i < num_possible_cluster; i++)
        {
            for(int j = 0; j < X.cols; ++j) {

                cv::Vec2d diffV2d = centroids.at<cv::Vec2d>((int) i, j) - old_centroids.at<cv::Vec2d>((int) i, j);

                squared_norm += pow(diffV2d[0], 2) + pow(diffV2d[1], 2);
            }
        }

        if(error < best_error){
            for (int i = 0; i < num_possible_cluster; i++)
            {
                for(int j = 0; j < X.cols; ++j) {
                    best_centroids.at<cv::Vec2d>(i, j) = centroids.at<cv::Vec2d>(i, j);
                }
            }

            for(int i = 0; i < labels.size(); i++)
            {
                best_labels[i] = labels[i];
            }

            best_error = error;
        }

        cout << iter_counter << ": " << error << endl;


        iter_counter++;

    } while ( sqrt(pow(error - old_error, 2)) > DBL_EPSILON && iter_counter < 300);


    TiXmlElement * labels_log = new TiXmlElement( "Labels" );

    ostringstream  labels_text;

    for(int i = 0; i < labels.size(); i++)
    {
        labels_text << labels[i] << ", ";
    }

    text = new TiXmlText( labels_text.str() );

    labels_log->LinkEndChild( text );

    elem->LinkEndChild(labels_log);


    cout_ << "final centroid" << endl;

    TiXmlElement * generation = new TiXmlElement( "Generation " );

    ostringstream  heatmap_string;

    for(int i = 0; i < num_possible_cluster; i++)
    {
        //translate back centroid i in original space
        for(int j = 0; j < X.cols; ++j) {
            best_centroids.at<cv::Vec2d>(i, j) += currentcentroid.at<cv::Vec2d>(0, j);
            heatmap_string << best_centroids.at<cv::Vec2d>(i, j) << ", ";
        }

        heatmap_string << "# ";

    }

    cout_ << "best centroids" << endl;
    cout_ << best_centroids << endl;

    prev_centroids.emplace_back(best_centroids);

    text = new TiXmlText( heatmap_string.str() );

    generation->LinkEndChild( text );

    elem->LinkEndChild(generation);

    //doc.SaveFile( "Centroids.xml" );
}

double KMeansModel::evaluate_surprise(std::shared_ptr<GeneticIndividual> &ind) {

    if (prev_centroids.size() >= 2 && predicted_centroids.size() > 0) {

        if (!ind->behavior.validBehavior) {
            return 0.0; // comparing two individuals from which one has no behavior, return 0.0
        } else if (ind->behavior.evaluationType == SET_NONE) {
            return 0.0;
        } else if (ind->behavior.evaluationType == SET_ABSDIF) {

            double diff = 0.0;

            //double min = numeric_limits<double>::max();

            double dist = 0.0;

            vector<double> distances;

            for(int j = 0; j < predicted_centroids.size(); ++j) {

                diff = 0.0;

                for (int i = 0; i < ind->behavior.signature->mat.rows; ++i) {

                    cv::Vec2d diffV2d =
                            predicted_centroids[j].at<cv::Vec2d>((int) i) - ind->behavior.signature->mat.at<cv::Vec2d>((int) i);
                    diff += sqrt(pow(diffV2d[0], 2) + pow(diffV2d[1], 2));

                }

                distances.emplace_back(diff);

                /*if(diff < min)
                {
                    min = diff;
                }*/
            }

            sort(distances.begin(), distances.end());

            for(int i = 0; i < 4; ++i) {
                dist += distances[i];
            }

            cout_ << "returning surprise = " << dist/4 << endl;

            return (double) dist/4;
        } else {
            std::cerr << "ERROR, UNKNOWN TYPE OF SIMILARITY TYPE" << std::endl;
            exit(79);
        }

    } else {
        cout_ << "returning surprise = " << 1.0 << endl;
        return 1.0;
    }
}

cv::Mat calculateCentroid(cv::Mat &trajectories)
{
    cv::Mat centroid = cv::Mat::zeros(1, trajectories.cols, CV_64FC2);

    for(int j = 0; j < trajectories.cols; ++j)
    {
        for(int i = 0; i < trajectories.rows; ++i)
        {

            centroid.at<cv::Vec2d>(0, j) += trajectories.at<cv::Vec2d>(i,j);

        }

        centroid.at<cv::Vec2d>(0, j)[0] /=  trajectories.rows;
        centroid.at<cv::Vec2d>(0, j)[1] /=  trajectories.rows;
    }

    return centroid;
}

