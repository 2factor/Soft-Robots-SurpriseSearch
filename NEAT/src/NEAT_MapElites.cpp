//
// Created by idgresearchp on 14/02/18.
//

#include "NEAT_MapElites.h"

MapElites::MapElites()
{
    //TODO get behavioural shape
    behav_shape[0] = int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1"));
    behav_shape[1] = int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2"));

    _array.resize(behav_shape);
    _array_parents.resize(behav_shape);
    _array_counter.resize(behav_shape);
    _array_selection.resize(behav_shape);
    _array_ids.resize(behav_shape);

    behav_shape_s[0] = int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1"));
    behav_shape_s[1] = int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1"));

    _array_t_2.resize(behav_shape_s);
    _array_t_1.resize(behav_shape_s);
    _array_pred.resize(behav_shape_s);


    vector<GeneticNodeGene> genes;

    std::ofstream m_OutFile;

    m_OutFile.open("experiment.txt", std::ios_base::app);

    ostringstream  experiment_log;

    if(SURPRISE)
    {
        experiment_log << "surprise" << endl;
    }

    if(SURPRISE_MODEL)
    {
        experiment_log << "HeatMap surprise" << endl;
    }

    if(SURPRISE_FITNESS_MODEL)
    {
        experiment_log << "HeatMap fitness surprise degree 100" << endl;
    }

    if(SURPRISE_BEHAVIOUR)
    {
        experiment_log << "kmeans surprise" << endl;
        //experiment_log << "heatmap behaviour surprise" << endl;
    }

    if(SURPRISE_FITNESS)
    {
        experiment_log << "fitness surprise" << endl;
    }

    if(NOVELTY)
    {
        experiment_log << "novelty" << endl;
    }

    if(NOV_SUR)
    {
        experiment_log << "kmeans surprise" << endl;
        experiment_log << "nov-sur" << endl;
    }

    if(MULTI_NOV_SUR)
    {
        experiment_log << "kmeans surprise" << endl;
        experiment_log << "multi nov-sur" << endl;
    }

    m_OutFile << experiment_log.str() << std::endl;

    m_OutFile.close();

    if(SURPRISE_BEHAVIOUR)
    {
        surprise_model = new KMeansModel(15);
        //surprise_model = new HeatMapModel();
    }

    if(NOV_SUR || MULTI_NOV_SUR)
    {
        surprise_model = new KMeansModel(15);
    }

    if(SURPRISE || SURPRISE_FITNESS)
    {
        for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i) {
            for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j) {

                behav_index_t behav_pos;
                behav_pos[0] = i;
                behav_pos[1] = j;

                _array_t_1(behav_pos) = 0;
                _array_t_2(behav_pos) = 0;
                _array_pred(behav_pos) = 0;

            }
        }
    }

    if(SOFT_ROBOTS)
    {

        // Network inputs
        genes.push_back(GeneticNodeGene("Bias","NetworkSensor",0,false));
        genes.push_back(GeneticNodeGene("x","NetworkSensor",0,false));
        genes.push_back(GeneticNodeGene("y","NetworkSensor",0,false));
        genes.push_back(GeneticNodeGene("z","NetworkSensor",0,false));

        if(bool(NEAT::Globals::getSingleton()->getParameterValue("SymmetryInputs")))
        {
            genes.push_back(GeneticNodeGene("d","NetworkSensor",0,false));
            genes.push_back(GeneticNodeGene("dxy","NetworkSensor",0,false));
            genes.push_back(GeneticNodeGene("dxz","NetworkSensor",0,false));
            genes.push_back(GeneticNodeGene("dyz","NetworkSensor",0,false));
        }

        // Network outputs
        genes.push_back(GeneticNodeGene("Output","NetworkOutputNode",1,false,ACTIVATION_FUNCTION_SIGMOID)); // is there a voxel?

        int num_materials = int(NEAT::Globals::getSingleton()->getParameterValue("MaterialsNum"));

        for (int i = 0; i < num_materials; ++i)
        {
            std::stringstream ss; ss << i;
            genes.push_back(GeneticNodeGene("OutputMat"+ss.str(),"NetworkOutputNode",1,false,ACTIVATION_FUNCTION_SIGMOID)); // Material to be used
            if(bool(NEAT::Globals::getSingleton()->getParameterValue("EvolveMaterialProperties")))
            {
                genes.push_back(GeneticNodeGene("OutputMatCTE"+ss.str(),"NetworkOutputNode",1,false,ACTIVATION_FUNCTION_SIGMOID)); // Material's CTE
                genes.push_back(GeneticNodeGene("OutputMatDensity"+ss.str(),"NetworkOutputNode",1,false,ACTIVATION_FUNCTION_SIGMOID)); // Material's density
                genes.push_back(GeneticNodeGene("OutputMatTempPhase"+ss.str(),"NetworkOutputNode",1,false,ACTIVATION_FUNCTION_SIGMOID)); // Material's temporary phase
                genes.push_back(GeneticNodeGene("OutputMatPoissonsRatio"+ss.str(),"NetworkOutputNode",1,false,ACTIVATION_FUNCTION_SIGMOID)); // Material's poissons ratio
            }
        }

        for (int a=0;a<int(NEAT::Globals::getSingleton()->getParameterValue("InitialPopulationSize"));a++)
        {
            std::shared_ptr<GeneticIndividual> individual(new GeneticIndividual(genes,true,1.0));

            for (int b=0;b<0;b++)
            {
                individual->testMutate();
            }

            population.emplace_back(individual);
            p_parents.emplace_back(individual);
        }

    }
    else {
        // Network inputs
        genes.push_back(GeneticNodeGene("Bias", "NetworkSensor", 0, false));
        genes.push_back(GeneticNodeGene("x", "NetworkSensor", 0, false));
        genes.push_back(GeneticNodeGene("y", "NetworkSensor", 0, false));

        if(bool(NEAT::Globals::getSingleton()->getParameterValue("SymmetryInputs")))
        {
            genes.push_back(GeneticNodeGene("d","NetworkSensor",0,false));

//            genes.push_back(GeneticNodeGene("d_first_p","NetworkSensor",0,false));
//
//            genes.push_back(GeneticNodeGene("d_second_p","NetworkSensor",0,false));
        }

        // Network outputs
//        genes.push_back(GeneticNodeGene("Output", "NetworkOutputNode", 1, false,
//                                        ACTIVATION_FUNCTION_SIGMOID)); // is there a voxel?
        for (int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MaterialsNum")); ++i) {
            std::stringstream ss;
            ss << i;
            genes.push_back(GeneticNodeGene("OutputMat" + ss.str(), "NetworkOutputNode", 1, false,
                                            ACTIVATION_FUNCTION_SIGMOID)); // Material to be used

        }
        // Create random initial population
        for (size_t a = 0; a < int(NEAT::Globals::getSingleton()->getParameterValue("InitialPopulationSize")); ++a) {
            std::shared_ptr<GeneticIndividual> individual(new GeneticIndividual(genes, true, 1.0));
            individual->individualID = "null";
            population.emplace_back(individual);
            p_parents.emplace_back(individual);
        }
    }

}

MapElites::~MapElites(){
    if(SURPRISE_MODEL)
        delete surprise_model;
    if(SURPRISE_BEHAVIOUR)
        delete surprise_model;
    if(NOV_SUR)
        delete surprise_model;
}

void MapElites::epoch(int onGeneration) {

    vector<std::shared_ptr<GeneticIndividual>> ptmp;

    population.clear();
    p_parents.clear();

    //collect elites
    for(int index = 0; index < _array.num_elements(); ++index)
        //check for presence of the elite individual
        if(_array.data()[index] && _array.data()[index]->isValid())
            ptmp.emplace_back(_array.data()[index]);

    for (size_t i = 0; i < NEAT::Globals::getSingleton()->getParameterValue("PopulationSize"); ++i) {
        indiv_t first_parent = _selection(ptmp);
        indiv_t second_parent = _selection(ptmp);

        _save_selection(first_parent);
        _save_selection(second_parent);

        population.emplace_back(std::shared_ptr<GeneticIndividual>(
                new GeneticIndividual(first_parent, second_parent))
        );

        population[population.size()-1]->individualID = "null";

        p_parents.emplace_back(first_parent);
    }

    assert(population.size() == NEAT::Globals::getSingleton()->getParameterValue("PopulationSize"));

}

void MapElites::update_archive()
{
    vector<std::shared_ptr<NEAT::GeneticIndividual> >::iterator tmpIterator, tmpIterator_parents;

    for (tmpIterator = population.begin(), tmpIterator_parents = p_parents.begin();
         tmpIterator != population.end(); ++tmpIterator, ++tmpIterator_parents) {

        _add_to_archive(*tmpIterator, *tmpIterator_parents);
    }

    if(NOVELTY|| NOV_SUR || MULTI_NOV_SUR) {
        //novelty

        vector<std::shared_ptr<GeneticIndividual> >::iterator tmpIterator;

        vector<std::shared_ptr<GeneticIndividual>> ptmp;

        //collect elites
        for(int index = 0; index < _array.num_elements(); ++index) {
            //check for presence of the elite individual
            if (_array.data()[index] && _array.data()[index]->isValid())
                ptmp.emplace_back(_array.data()[index]);
        }

        for (tmpIterator = population.begin();
             tmpIterator != population.end(); ++tmpIterator) {


            if (novelIndividuals.size() ==
                0) { // Novel individual size is zero, so this individual is the first novel we found
                (*tmpIterator)->noveltyMapElites =
                        double(NEAT::Globals::getSingleton()->getParameterValue(
                                "NoveltyThreshold")); // Value to be assigned to the first individual entering the novel individuals' group
            } else {
                (*tmpIterator)->noveltyMapElites = getIndividualNovelty(*tmpIterator, ptmp);
            }

            (*tmpIterator)->noveltyMapElites += 10e-5; // to avoid having zero fitness
            if ((*tmpIterator)->noveltyMapElites < 0.0 ||
                (*tmpIterator)->noveltyMapElites > numeric_limits<double>::max()) {
                cerr << "ERROR : Novelty value is less than zero or greater than the maximum double value." << endl;
                exit(82);
            }

            if ((*tmpIterator)->noveltyMapElites > double(NEAT::Globals::getSingleton()->getParameterValue("NoveltyThreshold"))) {
                (*tmpIterator)->isNovel(true);
                novelIndividuals.push_back((*tmpIterator));
            }
        }

        for(int index = 0; index < ptmp.size(); ++index) {

            ptmp.at(index)->noveltyMapElites = getIndividualNovelty(ptmp.at(index), ptmp);


        }

        if(novelIndividuals.size() >= 2500)
        {


            std::priority_queue<pair<double, std::shared_ptr<GeneticIndividual>>> sortedNovelIndividuals;

            for (int i = 0; i < novelIndividuals.size(); i++) {
                sortedNovelIndividuals.push(make_pair(novelIndividuals.at(i)->noveltyMapElites,
                                                      novelIndividuals.at(i)));
            }

            novelIndividuals.clear();

            //cout << "novelty individuals size " << novelIndividuals.size() << endl;

            for (int i = 0; i < 2500; i++) {
                //cout << "novelty top " << sortedNovelIndividuals.top().first << endl;
                novelIndividuals.emplace_back(sortedNovelIndividuals.top().second);
                sortedNovelIndividuals.pop();
            }

            //cout << "novelty individuals size " << novelIndividuals.size() << endl;

        }

        /*for (tmpIterator = population.begin();
             tmpIterator != population.end(); ++tmpIterator) {


            if (novelIndividuals.size() ==
                0) { // Novel individual size is zero, so this individual is the first novel we found
                (*tmpIterator)->noveltyMapElites =
                        double(NEAT::Globals::getSingleton()->getParameterValue(
                                "NoveltyThreshold")); // Value to be assigned to the first individual entering the novel individuals' group
            } else {
                (*tmpIterator)->noveltyMapElites = getIndividualNovelty(*tmpIterator, ptmp);
            }

            (*tmpIterator)->noveltyMapElites += 10e-5; // to avoid having zero fitness
            if ((*tmpIterator)->noveltyMapElites < 0.0 ||
                    (*tmpIterator)->noveltyMapElites > numeric_limits<double>::max()) {
                cerr << "ERROR : Novelty value is less than zero or greater than the maximum double value." << endl;
                exit(82);
            }

            if ((*tmpIterator)->noveltyMapElites > double(NEAT::Globals::getSingleton()->getParameterValue("NoveltyThreshold"))) {
                (*tmpIterator)->isNovel(true);
                novelIndividuals.push_back((*tmpIterator));
            }
        }*/
    }

    if(SURPRISE_BEHAVIOUR || NOV_SUR || MULTI_NOV_SUR)
    {
        iterations++;
        std::shared_ptr<GeneticGeneration> tmp_gen_ptr(new GeneticGeneration(0));
        vector<std::shared_ptr<GeneticIndividual> >::iterator tmpIterator;

        for (tmpIterator = population.begin();
             tmpIterator != population.end(); ++tmpIterator)
        {
            tmp_gen_ptr->addIndividual(*tmpIterator);
        }

//        for(int index = 0; index < _array.num_elements(); ++index)
//            //check for presence of the elite individual
//            if(_array.data()[index] && _array.data()[index]->isValid())
//                tmp_gen_ptr->addIndividual(_array.data()[index]);

        vector<std::shared_ptr<GeneticGeneration> > generations;

        generations.emplace_back(tmp_gen_ptr);

        surprise_model->updateModel(generations, 0);

        for (int index = 0; index < _array.num_elements(); ++index) {
            //check for presence of the elite individual
            if (_array.data()[index] && _array.data()[index]->isValid())
                _array.data()[index]->surpriseMapElites = surprise_model->evaluate_surprise(_array.data()[index]);
        }



    }

    if(MULTI_NOV_SUR)
    {
        vector<std::shared_ptr<GeneticIndividual> >::iterator tmpIterator;

        vector<std::shared_ptr<GeneticIndividual>> ptmp;

        //collect elites
        for(int index = 0; index < _array.num_elements(); ++index) {
            //check for presence of the elite individual
            if (_array.data()[index] && _array.data()[index]->isValid()) {
                ptmp.emplace_back(_array.data()[index]);
                _array.data()[index]->fitnesses[0] = _array.data()[index]->noveltyMapElites;
                _array.data()[index]->fitnesses[1] = _array.data()[index]->surpriseMapElites;
            }

        }

        sortNondominated(ptmp);
        calculateCrowdingDistance(ptmp);


    }

    if(SURPRISE_MODEL)
    {
        iterations++;
        for (tmpIterator = population.begin(), tmpIterator_parents = p_parents.begin();
             tmpIterator != population.end(); ++tmpIterator, ++tmpIterator_parents)

            _update_surprise_model(*tmpIterator);

        float sum = 0.0;

        for (int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i) {
            for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j) {

                behav_index_t behav_pos;
                behav_pos[0] = i;
                behav_pos[1] = j;

                sum += _array_t_1(behav_pos);

            }
        }

        for (int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i) {
            for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j) {

                behav_index_t behav_pos;
                behav_pos[0] = i;
                behav_pos[1] = j;

                _array_t_1(behav_pos) /= sum;

            }
        }


        surprise_model->updateModel(_array_t_1);

    }

    if(SURPRISE_FITNESS_MODEL)
    {
        iterations++;

        for (int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i) {
            for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j) {

                behav_index_t behav_pos;
                behav_pos[0] = i;
                behav_pos[1] = j;

                if (_array(behav_pos)) {
                    _array_t_1(behav_pos) = _array(behav_pos)->getFitness();
                } else {
                    _array_t_1(behav_pos) = 0;
                }

            }
        }

        float sum = 0.0;

        for (int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i) {
            for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j) {

                behav_index_t behav_pos;
                behav_pos[0] = i;
                behav_pos[1] = j;

                sum += _array_t_1(behav_pos);

            }
        }

        for (int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i) {
            for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j) {

                behav_index_t behav_pos;
                behav_pos[0] = i;
                behav_pos[1] = j;

                _array_t_1(behav_pos) /= sum;

            }
        }


        surprise_model->updateModel(_array_t_1);

    }

    if(SURPRISE || SURPRISE_FITNESS) {

        iterations += 1;

        //make prediction
        if (iterations > 2) {

            for (int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i) {
                for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j) {

                    behav_index_t behav_pos;
                    behav_pos[0] = i;
                    behav_pos[1] = j;

                    _array_pred(behav_pos) =
                            _array_t_1(behav_pos) * 2 - _array_t_2(behav_pos);

                    if (_array_pred(behav_pos) > 1) {
                        _array_pred(behav_pos) = 1;
                    }

                    if (_array_pred(behav_pos) < 0) {
                        _array_pred(behav_pos) = 0;
                    }

                }
            }
            /*
            cout << " debug surprise" << endl;
            cout << endl;

            std::cout << std::fixed;
            std::cout << std::setprecision(2);

            //debug model
            for (int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i) {
                for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j) {
                    behav_index_t behav_pos;
                    behav_pos[0] = i;
                    behav_pos[1] = j;

                    cout << _array_t_2(behav_pos) << " ";
                }
                cout << endl;
            }
            cout << " *****************************" << endl;
            for (int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i) {
                for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j) {
                    behav_index_t behav_pos;
                    behav_pos[0] = i;
                    behav_pos[1] = j;

                    cout << _array_t_1(behav_pos) << " ";
                }
                cout << endl;
            }
            cout << " *****************************" << endl;
            for (int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i) {
                for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j) {
                    behav_index_t behav_pos;
                    behav_pos[0] = i;
                    behav_pos[1] = j;

                    cout << _array_pred(behav_pos) << " ";
                }
                cout << endl;
            }

            cout << " *****************************" << endl;
             */
        }


        //surprise model update
        for (int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i) {
            for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j) {

                behav_index_t behav_pos;
                behav_pos[0] = i;
                behav_pos[1] = j;

                _array_t_2(behav_pos) = _array_t_1(behav_pos);
                _array_t_1(behav_pos) = 0;

            }
        }

        if (SURPRISE) {

            for (tmpIterator = population.begin(), tmpIterator_parents = p_parents.begin();
                 tmpIterator != population.end(); ++tmpIterator, ++tmpIterator_parents)

                _update_surprise_model(*tmpIterator);

            /*for (int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i) {
                for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j) {

                    behav_index_t behav_pos;
                    behav_pos[0] = i;
                    behav_pos[1] = j;

                    if (_array(behav_pos)) {
                        _array_t_1(behav_pos) = _array_counter(behav_pos);
                    } else {
                        _array_t_1(behav_pos) = 0;
                    }

                }
            }*/
        } else {


            for (int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i) {
                for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j) {

                    behav_index_t behav_pos;
                    behav_pos[0] = i;
                    behav_pos[1] = j;

                    if (_array(behav_pos)) {
                        _array_t_1(behav_pos) = _array(behav_pos)->getFitness();
                    } else {
                        _array_t_1(behav_pos) = 0;
                    }

                }
            }
        }

        float sum = 0.0;

        for (int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i) {
            for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j) {

                behav_index_t behav_pos;
                behav_pos[0] = i;
                behav_pos[1] = j;

                sum += _array_t_1(behav_pos);

            }
        }

        for (int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i) {
            for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j) {

                behav_index_t behav_pos;
                behav_pos[0] = i;
                behav_pos[1] = j;

                _array_t_1(behav_pos) /= sum;

            }
        }


        printPredictions();

    }



    //debug
    for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1")); ++i)
    {
        for(int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2")); ++j)
        {
            behav_index_t behav_pos;
            behav_pos[0] = i;
            behav_pos[1] = j;

            if(_array(behav_pos))
                cout << _array(behav_pos)->getFitness() << " ";
            else
                cout << -0.01f << " ";
        }

        cout << endl;
    }

    cout << endl;
    cout << endl;

    for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1")); ++i)
    {
        for(int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2")); ++j)
        {
            behav_index_t behav_pos;
            behav_pos[0] = i;
            behav_pos[1] = j;

            if(_array(behav_pos))
                cout << _array_counter(behav_pos) << " ";
            else
                cout << 0 << " ";
        }

        cout << endl;
    }
}

void MapElites::printPredictions() {

    std::ofstream m_OutFile;

    m_OutFile.open("prediction_log.txt", std::ios_base::app);

    ostringstream  elites_log;

    std::shared_ptr<GeneticIndividual> best_ind;

    elites_log << std::fixed;
    elites_log << std::setprecision(4);

    elites_log << iterations << endl;

    elites_log << endl;

    //debug
    for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i)
    {
        for(int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j)
        {
            behav_index_t behav_pos;
            behav_pos[0] = i;
            behav_pos[1] = j;

            elites_log << _array_t_2(behav_pos) << " ";

        }

        elites_log << endl;
    }

    elites_log << "**********************************************" << endl;

    for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i)
    {
        for(int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j)
        {
            behav_index_t behav_pos;
            behav_pos[0] = i;
            behav_pos[1] = j;

            elites_log << _array_t_1(behav_pos) << " ";

        }

        elites_log << endl;
    }

    elites_log << "**********************************************" << endl;

    for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++i)
    {
        for(int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SUR_SHAPE_1")); ++j)
        {
            behav_index_t behav_pos;
            behav_pos[0] = i;
            behav_pos[1] = j;

            elites_log << _array_pred(behav_pos) << " ";

        }

        elites_log << endl;
    }

    elites_log << "**********************************************" << endl;

    m_OutFile << elites_log.str() << std::endl;


    m_OutFile.close();
}

void MapElites::printElites(int gen = -1) {

    std::ofstream m_OutFile;

    if (gen == -1){
        m_OutFile.open("final_elites_log.txt", std::ios::out);
    } else{
        ostringstream  file_name;
        file_name << "elites_log_" << gen << ".txt";

        m_OutFile.open(file_name.str(), std::ios::out);
    }

    ostringstream  elites_log;

    std::shared_ptr<GeneticIndividual> best_ind;

    elites_log << "*********************" << endl;

    for (int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1")); ++i) {
        for (int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2")); ++j) {
            behav_index_t behav_pos;
            behav_pos[0] = i;
            behav_pos[1] = j;

            if (_array(behav_pos)) {

                best_ind = _array(behav_pos);

                elites_log << i << " " << j << endl;

                elites_log << best_ind << endl;

                elites_log << best_ind->getFitness() << endl;

                elites_log << "-----------------------" << endl;
            }
        }
    }

    elites_log << "**********************************************" << endl;

    elites_log << std::fixed;
    elites_log << std::setprecision(2);

    //debug
    for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1")); ++i)
    {
        for(int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2")); ++j)
        {
            behav_index_t behav_pos;
            behav_pos[0] = i;
            behav_pos[1] = j;

            if(_array(behav_pos))
                elites_log << _array(behav_pos)->getFitness() << " ";
            else
                elites_log << -0.01f << " ";
        }

        elites_log << endl;
    }

    elites_log << "**********************************************" << endl;

    for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1")); ++i)
    {
        for(int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2")); ++j)
        {
            behav_index_t behav_pos;
            behav_pos[0] = i;
            behav_pos[1] = j;

            if(_array(behav_pos))
                elites_log << _array_counter(behav_pos) << " ";
            else
                elites_log << 0 << " ";
        }

        elites_log << endl;
    }

    elites_log << "**********************************************" << endl;

    for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1")); ++i)
    {
        for(int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2")); ++j)
        {
            behav_index_t behav_pos;
            behav_pos[0] = i;
            behav_pos[1] = j;

            if(_array(behav_pos))
                elites_log << _array_selection(behav_pos) << " ";
            else
                elites_log << 0 << " ";
        }

        elites_log << endl;
    }

    m_OutFile << elites_log.str() << std::endl;


    m_OutFile.close();
}

void MapElites::generationData(int onGeneration) {
    double championFitness = 0.0;
    double totalIndividualFitness = 0.0;


    for(int i = 0; i < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1")); ++i)
    {
        for(int j = 0; j < int(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2")); ++j)
        {
            behav_index_t behav_pos;
            behav_pos[0] = i;
            behav_pos[1] = j;

            if(_array(behav_pos))
            {
                totalIndividualFitness += _array(behav_pos)->getFitness();

                if(_array(behav_pos)->getFitness() > championFitness)
                    championFitness = _array(behav_pos)->getFitness();
            }

        }

    }


    double averageFitness = totalIndividualFitness /
                            (double(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_1"))*double(NEAT::Globals::getSingleton()->getParameterValue("MAP_SHAPE_2")));


    if (bool(NEAT::Globals::getSingleton()->getParameterValue("RecordStats"))) {
        // Write statistics about current generation
        ofstream statsFile(NEAT::Globals::getSingleton()->getRootFilename() + "stats.dat", std::ios_base::app);
        std::ostringstream output;
        if (statsFile.is_open()) {


            output << "Gen: " << int(onGeneration + 1)
                   << ", Avg. Fit: " << averageFitness
                   << ", Best ever Fit: " << championFitness;

            statsFile << int(onGeneration + 1)
                      << "," << averageFitness
                      << "," << championFitness;


            statsFile << "\n";
        }
        cout << output.str() << endl;
        statsFile.close();
    }

    return;
}



//long int MapElites::getindex(const array_t & m, const individual_ptr_t* requestedElement, const unsigned short int direction) const {
//    int offset = requestedElement - m.origin();
//    return (offset / m.strides()[direction] % m.shape()[direction] +  m.index_bases()[direction]);
//}

//behav_index_t MapElites::getindexarray(const array_t & m, const individual_ptr_t* requestedElement ) const {
//    behav_index_t _index;
//    for (unsigned int dir = 0; dir < BEHAVIOURAL_DIM; dir++ ) {
//        _index[dir] = getindex( m, requestedElement, dir );
//    }
//
//    return _index;
//}

//const array_t& MapElites::archive() const {
//    return _array;
//}
//const array_t& MapElites::parents() const {
//    return _array_parents;
//}
//
//template<typename I>
//point_t MapElites::get_point(const I& indiv) const {
//    return _get_point(indiv);
//}

