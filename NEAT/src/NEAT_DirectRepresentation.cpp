//
// Created by idgresearch on 12/06/18.
//

#include "NEAT_DirectRepresentation.h"
#include <algorithm>    // std::swap

namespace NEAT {

    DirectRepresentation::DirectRepresentation()
    :
    fitness(0)
    {

        int SIZE_X = uint(NEAT::Globals::getSingleton()->getParameterValue("NumMapX"));
        int SIZE_Y = uint(NEAT::Globals::getSingleton()->getParameterValue("NumMapY"));
        int num_materials = uint(NEAT::Globals::getSingleton()->getParameterValue("MaterialsNum"));

        for (int i = 0; i < SIZE_X * SIZE_Y; ++i)
        {
            if (Globals::getSingleton()->getRandom().getRandomDouble() < 0.5)
                genome.emplace_back(0);
            else
                genome.emplace_back(1 + Globals::getSingleton()->getRandom().getRandomInt(num_materials-1));

        }


    }

    DirectRepresentation::DirectRepresentation(DirectRepresentation &copy)
    {
        fitness = copy.fitness;

        genome = copy.genome;
    }

    DirectRepresentation::~DirectRepresentation()
    {
    }

    void DirectRepresentation::cross(std::shared_ptr<DirectRepresentation> parent2,
                                     std::shared_ptr<DirectRepresentation> child1,
                                     std::shared_ptr<DirectRepresentation> child2)
    {

        double cross_prob = double(Globals::getSingleton()->getParameterValue("CrossProbability"));

        child1->genome = genome;
        child2->genome = parent2->genome;

        int SIZE_X = uint(NEAT::Globals::getSingleton()->getParameterValue("NumMapX"));
        int SIZE_Y = uint(NEAT::Globals::getSingleton()->getParameterValue("NumMapY"));

        if (Globals::getSingleton()->getRandom().getRandomDouble() < cross_prob) {

            int start_index = Globals::getSingleton()->getRandom().getRandomInt(SIZE_X * SIZE_Y);

            int end_index = Globals::getSingleton()->getRandom().getRandomInt(SIZE_X * SIZE_Y);

            if (start_index > end_index) {
                std::swap(start_index, end_index);
            }

            for (int i = start_index; i < end_index; ++i) {
                child1->genome[i] = parent2->genome[i];

                child2->genome[i] = genome[i];
            }
        }



    }

    void DirectRepresentation::mutate()
    {

        int SIZE_X = uint(NEAT::Globals::getSingleton()->getParameterValue("NumMapX"));
        int SIZE_Y = uint(NEAT::Globals::getSingleton()->getParameterValue("NumMapY"));

        double mutate_prob = double(Globals::getSingleton()->getParameterValue("MutateProbability"));

//        for(int i = 0; i < genome.size(); ++i)
//        {
//            cout << genome[i] << " ";
//        }
//
//        cout << endl;

        for (int i = 0; i < genome.size(); ++i)
        {

            if (Globals::getSingleton()->getRandom().getRandomDouble() < mutate_prob)
            {
                //switch from passable to impassable tile and viceversa
                if (Globals::getSingleton()->getRandom().getRandomDouble() < 0.5)
                {
                    if(genome[i] != 0)
                    {
                        genome[i] = 0;
                    }
                    else
                    {
                        genome[i] = 1;
                    }
                }
                else
                {
                    //swap two adjenct tiles

                    int option = Globals::getSingleton()->getRandom().getRandomInt(4);


                    switch (option)
                    {
                        case 0:
                            if(i-1 > 0) {
                                std::swap(genome[i - 1], genome[i]);
                            } else{
                                std::swap(genome[SIZE_X*SIZE_Y-1], genome[i]);
                            }
                            break;
                        case 1:
                            std::swap(genome[(i + 1)%(SIZE_X*SIZE_Y)], genome[i]);
                            break;
                        case 2:
                            if(i - SIZE_Y > 0) {
                                std::swap(genome[i - SIZE_Y], genome[i]);
                            }
                            else{
                                std::swap(genome[SIZE_X * SIZE_Y - SIZE_Y + i], genome[i]);
                            }
                            break;
                        case 3:
                            std::swap(genome[(i + SIZE_Y)%(SIZE_X*SIZE_Y)], genome[i]);
                            break;
                    }
                }


            }

        }

//        for(int i = 0; i < genome.size(); ++i)
//        {
//            cout << genome[i] << " ";
//        }
//
//        cout << endl;

    }
}